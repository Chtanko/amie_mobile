import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'controller/evenements_controller.dart';
import 'controller/auth_controller.dart';
import 'views/home/home_screen.dart';
import 'views/onboarding_screen.dart';
import 'package:google_fonts/google_fonts.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);



  @override
  Widget build(BuildContext context) {
    final EvenementsController evenementsController = Get.put(EvenementsController());

    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.green, // This is your primary color
        primaryColor: const Color(0xFF294218), // This is the color you asked
        textTheme: GoogleFonts.latoTextTheme(
          Theme.of(context).textTheme,
        ),
      ),
      locale: const Locale('fr', 'FR'), // This is the locale configuration
      localizationsDelegates: const [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: const [
        Locale('fr', 'FR'), // Supported language
      ],
      title: 'AMIE',
      initialBinding: BindingsBuilder(() {
        Get.put(AuthController());
      }),
      home: const OnBoardingScreen(),
    );
  }
}

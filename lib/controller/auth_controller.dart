import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../views/home/home_screen.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'evenements_controller.dart';


class User {
  int idUtilisateur;
  String nom;
  String prenom;

  User({required this.idUtilisateur, required this.nom, required this.prenom});
}

class AuthController extends GetxController {
  var isLoading = false.obs;
  RxString accessToken = RxString('');
  Rx<User> loggedInUser = Rx<User>(User(idUtilisateur: 0, nom: '', prenom: ''));

  @override
  void onInit() {
    super.onInit();
    Get.put(EvenementsController());
  }


  Future<void> login({String? email, String? password}) async {
    isLoading(true);

    try {
      final response = await http.post(
        Uri.parse('http://amie.labinno-mtech.fr/api/authentification/login'),
        headers: {
          'Content-Type': 'application/json'
        },
        body: jsonEncode({'mail': email, 'motDePasse': password}),
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        String token = responseBody['token'];

        accessToken.value = token;
        await fetchLoggedInUserDetails(token , email!);

        isLoading(false);

        Get.offAll(() => const HomeScreen());

      } else {
        isLoading(false);
        Get.snackbar(
            'Erreur',
            'Échec de la connexion',
            colorText: Colors.white,
            backgroundColor: const Color(0xFF6AA517));
      }
    } catch (e) {
      isLoading(false);
      Get.snackbar(
          'Erreur',
          'Erreur lors de la connexion',
          colorText: Colors.white,
          backgroundColor: const Color(0xFF6AA517));
    }
  }

  Future<void> signUp({String? name, String? surname, String? email, String? password}) async {

    isLoading(true);

    try {
      final response = await http.post(
        Uri.parse('http://amie.labinno-mtech.fr/api/authentification/inscription'),
        headers: {'Content-Type': 'application/json'},
        body: jsonEncode({
          'nom': name,
          'prenom': surname,
          'mail': email,
          'motDePasse': password,
        }),
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        String token = responseBody['token'];

        isLoading(false);
        //Get.to(() => ProfileScreen());
      } else {
        isLoading(false);
        Get.snackbar(
            'Erreur',
            "Impossible de créer l'utilisateur",
            colorText: Colors.white,
            backgroundColor: const Color(0xFF6AA517));
      }
    } catch (e) {
      isLoading(false);
      Get.snackbar(
          'Erreur',
          "Erreur lors de l'inscription",
          colorText: Colors.white,
          backgroundColor: const Color(0xFF6AA517));
    }
  }

  void forgetPassword(String email) {
  }

  Future<void> fetchLoggedInUserDetails(String accessToken, String email) async {
    try {
      final response = await http.get(
        Uri.parse('http://amie.labinno-mtech.fr/api/utilisateurs/getallutilisateurs'),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json',
          'Authorization': accessToken,
        },
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        var loggedInUserDetails = responseBody.firstWhere((user) => user['mail'] == email);

        int idUtilisateur = loggedInUserDetails['idUtilisateur'];
        String nom = loggedInUserDetails['nom'];
        String prenom = loggedInUserDetails['prenom'];

        loggedInUser.value = User(idUtilisateur: idUtilisateur, nom: nom, prenom: prenom);
      } else {
        throw Exception('Impossible de récupérer les informations de l\'utilisateur');
      }
    } catch (e) {
      throw Exception('Erreur lors de la récupération des informations de l\'utilisateur');
    }
  }


}

var isProfileInformationLoading = false.obs;

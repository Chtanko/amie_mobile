import 'dart:convert';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' show jsonDecode, utf8;

class EvenementsController extends GetxController {
  var isLoading = false.obs;
  RxString accessToken = RxString('');

  Future<List> getAllEvenements(String accessToken) async {

    try {
      final response = await http.get(
        Uri.parse('http://amie.labinno-mtech.fr/api/evenement/getallevenements'),
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json',
          'Authorization': accessToken,
        },
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(utf8.decode(response.bodyBytes));
        print('Événements récupérés : $responseBody');
        return responseBody;
      } else {
        print('Erreur: StatusCode ${response.statusCode}');
        print('Erreur: ResponseBody ${response.body}');
        throw Exception('Impossible de récupérer les événements');
      }
    } catch (e) {
      print('Erreur: $e');
      throw Exception('Erreur lors de la récupération des événements');
    }
  }

}

var isProfileInformationLoading = false.obs;

import 'dart:convert';
import 'package:get/get_rx/src/rx_types/rx_types.dart';
import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:http/http.dart' as http;

class UtilisateurController extends GetxController {
  var isLoading = false.obs;
  RxString accessToken = RxString('');

    Future<List> getAllUsers(String accessToken) async {

    try {
      final response = await http.get(
        Uri.parse('http://amie.labinno-mtech.fr/api/utilisateurs/getallutilisateurs'),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json',
          'Authorization': accessToken,
        },
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        print('Utilisateurs récupérés : $responseBody');
        return responseBody;
      } else {
        print('Erreur: StatusCode ${response.statusCode}');
        print('Erreur: ResponseBody ${response.body}');
        throw Exception('Impossible de récupérer les utilisateurs');
      }
    } catch (e) {
      print('Erreur: $e');
      throw Exception('Erreur lors de la récupération des utilisateurs');
    }
  }

  Future<List> getAllUsersWithRoles(String accessToken) async {

    try {
      final response = await http.get(
        Uri.parse('http://amie.labinno-mtech.fr/api/utilisateurs/getallutilisateursavecroles'),
        headers: {
          'Content-Type': 'application/json; charset=UTF-8',
          'Accept': 'application/json',
          'Authorization': accessToken,
        },
      );

      if (response.statusCode == 200) {
        var responseBody = jsonDecode(response.body);
        print('Utilisateurs avec leurs rôles récupérés : $responseBody');
        return responseBody;
      } else {
        print('Erreur: StatusCode ${response.statusCode}');
        print('Erreur: ResponseBody ${response.body}');
        throw Exception('Impossible de récupérer les utilisateurs avec rôles');
      }
    } catch (e) {
      print('Erreur: $e');
      throw Exception('Erreur lors de la récupération des utilisateurs avec rôles');
    }
  }

  String getUserRole(List<dynamic> usersWithRoles, int userId) {
    final user = usersWithRoles.firstWhere((user) => user['idUtilisateur'] == userId);
    return user['roles'][0]['libelle'];
  }

}

var isProfileInformationLoading = false.obs;

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/cupertino.dart';

class TypologieEvenements extends StatefulWidget {
  const TypologieEvenements({Key? key}) : super(key: key);

  @override
  State<TypologieEvenements> createState() => TypologieEvenementsState();
}

class TypologieEvenementsState extends State<TypologieEvenements> {
  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }

  static Future<List> getTypologieEvenements() async {
    const String url = "http://amie.labinno-mtech.fr/api/typologieevenement/getalltypologieevenements";

    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      var data = json.decode(response.body);
      List<String> labels = data.map<String>((item) => item['label']).toList();
      return labels;
    } else {
      throw Exception('Erreur de chargement typologie evenements');
    }
  }
}

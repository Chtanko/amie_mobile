import 'package:ems/views/message/messenger_view.dart';
import 'package:ems/views/profile/profile_page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';

import '../../controller/auth_controller.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../contributions/contributions_view.dart';
import '../event/create_event.dart';
import '../faq/faq_view.dart';
import '../favourites/favourite_view.dart';
import '../home/home_screen.dart';

class ProfileView extends StatefulWidget {
  const ProfileView({Key? key}) : super(key: key);

  @override
  State<ProfileView> createState() => _ProfileViewState();
}

class _ProfileViewState extends State<ProfileView> {
  late AuthController _authController;

  int _selectedIndex = 3;
  bool _isItemSelected = true;

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
        () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
        () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
        () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
        () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      if (_isItemSelected && _selectedIndex == index) {
        _isItemSelected = false;
      } else {
        _isItemSelected = true;
        _selectedIndex = index;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack( // Utilisation de Stack ici
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/background.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      IconButton(
                        icon: const Icon(Icons.arrow_back),
                        onPressed: () => Get.back(),
                      ),
                      const Text(
                        'Mon profil',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.w300,
                            fontFamily: 'Inter'),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),

                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width *
                            0.2), // Ajouté pour l'espacement de 30%
                    child: Row(
                      children: <Widget>[
                        const Icon(
                          Icons.person,
                          color: Colors.lightGreen,
                        ),
                        const SizedBox(width: 10),
                        Expanded( // Utilisation de Expanded ici
                          child: TextButton(
                            onPressed: () {
                              Get.to(
                                    () => const ProfilePage(),
                                transition: Transition.fadeIn,
                                duration: const Duration(milliseconds: 500),
                              );
                            },
                            child: const Text(
                              'Mes informations',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Inter'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width *
                            0.2), // Ajouté pour l'espacement de 30%
                    child: Row(
                      children: <Widget>[
                        const Icon(
                          Icons.question_mark_rounded,
                          color: Colors.lightGreen,
                        ),
                        const SizedBox(width: 10),
                        Expanded( // Utilisation de Expanded ici
                          child: TextButton(
                            onPressed: () {
                              Get.to(
                                    () => const Faq(),
                                transition: Transition.fadeIn,
                                duration: const Duration(milliseconds: 500),
                              );
                            },
                            child: const Text(
                              'FAQ',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Inter'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(
                        horizontal: MediaQuery.of(context).size.width *
                            0.2), // Ajouté pour l'espacement de 30%
                    child: Row(
                      children: <Widget>[
                        const Icon(
                          Icons.message,
                          color: Colors.lightGreen,
                        ),
                        const SizedBox(width: 10),
                        Expanded( // Utilisation de Expanded ici
                          child: TextButton(
                            onPressed: () {
                              Get.to(
                                    () => const Messenger(),
                                transition: Transition.fadeIn,
                                duration: const Duration(milliseconds: 500),
                              );
                            },
                            child: const Text(
                              'Contactez nous',
                              style: TextStyle(
                                  fontSize: 20,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Inter'),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomNavBar(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      floatingActionButton: SizedBox(
        width: 40, // Largeur personnalisée
        height: 40, // Hauteur personnalisée
        child: FloatingActionButton(
          backgroundColor: const Color(0xFF4C780E),
          onPressed: () {
            Get.to(
                  () => const CreateEvent(),
              transition: Transition.fadeIn,
              duration: const Duration(milliseconds: 500),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

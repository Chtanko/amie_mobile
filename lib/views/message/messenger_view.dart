import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/auth_controller.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../../widgets/custom_dialog.dart';
import '../contributions/contributions_view.dart';
import '../event/create_event.dart';
import '../favourites/favourite_view.dart';
import '../home/home_screen.dart';
import '../profile/profile_view.dart';
import 'package:http/http.dart' as http;

class Messenger extends StatefulWidget {
  const Messenger({super.key});

  @override
  State<Messenger> createState() => _MessengerState();
}

class _MessengerState extends State<Messenger> {
  late AuthController _authController;
  int _selectedIndex = 3;
  List<Map<String, dynamic>> faqData = [];
  final TextEditingController _objetController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
        () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
        () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
        () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
        () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> sendMessage() async {
    if (_formKey.currentState!.validate()) {
      var customDialog = CustomDialog(context);
      bool? confirmationResult = await customDialog.displayConfirmationDialog(
        'Confirmation',
        'Êtes-vous sûr de vouloir envoyer le message ?',
      );
      if (confirmationResult == true) {
        var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
        final String url =
            "http://amie.labinno-mtech.fr/api/messagecontactcontroller/createmessagecontact/$idUtilisateur";
        AuthController authController = Get.find<AuthController>();

        final response = await http.post(
          Uri.parse(url),
          headers: {
            'Authorization': authController.accessToken.value,
            'accept': '*/*',
            'Content-Type': 'application/json'
          },
          body: json.encode({
            'objet': _objetController.text,
            'description': _descriptionController.text,
          }),
        );

        if (response.statusCode == 200) {
          print("Message envoyé avec succès");
          await customDialog.displayDialog(
            'Succès',
            'Message envoyé avec succès.',
          );
          _objetController.clear();
          _descriptionController.clear();
        } else {
          throw Exception('Erreur d\envoi de message');
        }
      }
    }
  }


  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
  }

  @override
  Widget build(BuildContext context) {
    final isKeyboardVisible = MediaQuery.of(context).viewInsets.bottom != 0;
    return DefaultTextStyle(
      style: const TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.w600,
        fontFamily: 'Inter',
      ),
      child: Scaffold(
        resizeToAvoidBottomInset: true, // Modifié pour éviter que le clavier ne recouvre le contenu
        body: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.arrow_back),
                            onPressed: () => Get.back(),
                          ),
                          const Text(
                            'Retour',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Inter'),
                          ),
                        ],
                      ),
                      const SizedBox(height: 18),
                      const Center(
                        child: Padding(
                          padding: EdgeInsets.only(top: 20.0),
                          child: Text(
                            'Rédiger un message',
                            style: TextStyle(
                              fontSize: 24,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(height: 20.0),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            horizontal:
                                MediaQuery.of(context).size.width * 0.1),
                        child: Column(
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 10.0),
                              child: TextFormField(
                                controller: _objetController,
                                decoration: InputDecoration(
                                  labelText: 'Objet',
                                  contentPadding: EdgeInsets.all(10),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                validator: (value) {
                                  if (value == null || value.isEmpty) {
                                    return 'Veuillez rédiger un objet';
                                  }
                                  return null;
                                },
                              ),
                            ),
                            const SizedBox(height: 18.0),
                            LayoutBuilder(
                              builder: (context, constraints) {
                                return Padding(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: constraints.maxWidth * 0.05),
                                  child: TextFormField(
                                    maxLines: 10,
                                    controller: _descriptionController,
                                    decoration: InputDecoration(
                                      labelText: 'Votre mesage',
                                      contentPadding: const EdgeInsets.all(10),
                                      border: OutlineInputBorder(
                                        borderRadius: BorderRadius.circular(15),
                                        borderSide: const BorderSide(
                                          color: Colors.grey,
                                          width: 1.0,
                                        ),
                                      ),
                                      labelStyle: const TextStyle(),
                                      floatingLabelBehavior:
                                          FloatingLabelBehavior.always,
                                    ),
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return 'Veuillez écrire un message';
                                      }
                                      return null;
                                    },
                                  ),
                                );
                              },
                            ),
                            const SizedBox(height: 18.0),
                            SizedBox(
                              width: 150,
                              height: 54,
                              child: ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: const Color(0xFF6AA517),
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                                onPressed: sendMessage,
                                child: const Text(
                                  "Envoyer",
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Colors.white,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavBar(
          selectedIndex: _selectedIndex,
          onItemTapped: _onItemTapped,
        ),
        floatingActionButton: isKeyboardVisible
            ? null
            : SizedBox(
          width: 40,
          height: 40,
          child: FloatingActionButton(
            backgroundColor: const Color(0xFF4C780E),
            onPressed: () {
              Get.to(
                    () => const CreateEvent(),
                transition: Transition.fadeIn,
                duration: const Duration(milliseconds: 500),
              );
            },
            child: const Icon(Icons.add),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/auth_controller.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../contributions/contributions_view.dart';
import '../event/create_event.dart';
import '../favourites/favourite_view.dart';
import '../home/home_screen.dart';
import '../profile/profile_view.dart';
import 'package:http/http.dart' as http;

class Faq extends StatefulWidget {
  const Faq({super.key});

  @override
  State<Faq> createState() => _FaqState();
}

class _FaqState extends State<Faq> {
  late AuthController _authController;
  int _selectedIndex = 3;
  List<Map<String, dynamic>> faqData = [];

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
            () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
            () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
            () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
            () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  Future<void> getAllFAQ() async {
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    const String url = "http://amie.labinno-mtech.fr/api/faq/getallfaq";
    AuthController authController = Get.find<AuthController>();

    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      setState(() {
        var decodedResponse = utf8.decode(response.bodyBytes);
        faqData = List<Map<String, dynamic>>.from(json.decode(decodedResponse));
      });
    } else {
      throw Exception('Erreur de chargement des FAQ');
    }
  }

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
    getAllFAQ();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTextStyle(
      style: const TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.w600,
        fontFamily: 'Inter',
      ),
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("assets/background.png"),
                  fit: BoxFit.cover,
                ),
              ),
            ),
            SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(top: 40.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        IconButton(
                          icon: const Icon(Icons.arrow_back),
                          onPressed: () => Get.back(),
                        ),
                        const Text(
                          'Retour',
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w300,
                              fontFamily: 'Inter'),
                        ),
                      ],
                    ),
                    const SizedBox(height: 18),
                    const Center(
                      child: Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child: Text(
                          'Foire aux questions',
                          style: TextStyle(
                            fontSize: 24,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                      child: ListView.builder(
                        shrinkWrap: true,
                        physics: const NeverScrollableScrollPhysics(),
                        itemCount: faqData.length,
                        itemBuilder: (context, index) {
                          return ExpansionTile(
                            title: Text(faqData[index]['question']),
                            children: [
                              Container(
                                color: Colors.black12,
                                padding: const EdgeInsets.all(20),
                                width: double.infinity,
                                child: Text(faqData[index]['description']),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        bottomNavigationBar: BottomNavBar(
          selectedIndex: _selectedIndex,
          onItemTapped: _onItemTapped,
        ),
        floatingActionButton: SizedBox(
          width: 40,
          height: 40,
          child: FloatingActionButton(
            backgroundColor: const Color(0xFF4C780E),
            onPressed: () {
              Get.to(
                    () => const CreateEvent(),
                transition: Transition.fadeIn,
                duration: const Duration(milliseconds: 500),
              );
            },
            child: const Icon(Icons.add),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      ),
    );
  }
}

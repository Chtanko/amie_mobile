import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../controller/auth_controller.dart';
import '../../widgets/AnimatedToggle.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../../widgets/event_widget.dart';
import '../event/create_event.dart';
import '../event/detailed_event.dart';
import '../favourites/favourite_view.dart';
import '../home/home_screen.dart';
import '../profile/profile_view.dart';
import 'package:http/http.dart' as http;

class ContributionsScreen extends StatefulWidget {
  const ContributionsScreen({Key? key}) : super(key: key);

  @override
  State<ContributionsScreen> createState() => _ContributionsScreenState();
}

class _ContributionsScreenState extends State<ContributionsScreen> {
  late AuthController _authController;
  int _selectedIndex = 2;
  bool _showUpcomingEvents = true;

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
            () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
            () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
            () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
            () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  List<dynamic> filterUpcomingEvents(List<dynamic> events) {

    var now = DateTime.now();
    return events.where((event) {
      print('Event: $event'); // Ajoutez cette ligne pour imprimer chaque événement
      if (event['date_debut'] == null || event['date_debut'].trim().isEmpty) {
        return false; // Ignore cet événement si 'date_debut' est null ou une chaîne vide
      }
      try {
        var eventDate = DateFormat("dd/MM/yyyy").parse(event['date_debut']);
        return eventDate.isAfter(now);
      } catch (FormatException) {
        print('Invalid date format for event ${event['name']}');
        return false; // Ignore cet événement si 'date_debut' n'est pas une date valide
      }
    }).toList();
  }

  List<dynamic> filterPastEvents(List<dynamic> events) {

    var now = DateTime.now();
    return events.where((event) {
      print('Event: $event'); // Add this line to print each event
      if (event['date_fin'] == null || event['date_fin'].trim().isEmpty) {
        return false; // Ignore this event if 'date_fin' is null or an empty string
      }
      try {
        var eventEndDate = DateFormat("dd/MM/yyyy").parse(event['date_fin']);
        return eventEndDate.isBefore(now);
      } catch (FormatException) {
        print('Invalid date format for event ${event['name']}');
        return false; // Ignore this event if 'date_fin' is not a valid date
      }
    }).toList();
  }

  Future<List<dynamic>> getContributionsById() async {
    var now = DateTime.now();
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;

    final String url = "http://amie.labinno-mtech.fr/api/evenement/$idUtilisateur";
    AuthController authController = Get.find<AuthController>();

    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse;
    } else {
      throw Exception('Erreur de chargement des inscriptions aux événements');
    }
  }

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
    getContributionsById();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () => Get.back(),
                  ),
                  const Text(
                    'Mes contributions',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Inter'),
                  ),
                ],
              ),

              const SizedBox(height: 20),

              LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double toggleButtonWidth = constraints.maxWidth * 0.7; // 70% de la largeur totale

                  return Center(
                    child: AnimatedToggle(
                      onToggle: (bool isUpcomingSelected) {
                        setState(() {
                          _showUpcomingEvents = isUpcomingSelected;
                        });
                      },
                    ),
                  );
                },
              ),
              const SizedBox(height: 8),

              Expanded(
                child: FutureBuilder<List<dynamic>>(
                  future: getContributionsById(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(child: CircularProgressIndicator());
                    } else if (snapshot.hasError) {
                      return Text("Erreur: ${snapshot.error}");
                    } else {
                      var allEvents = snapshot.data!;
                      var upcomingEvents = filterUpcomingEvents(allEvents);
                      var pastEvents = filterPastEvents(allEvents);
                      var eventsToShow = _showUpcomingEvents ? upcomingEvents : pastEvents;

                      if (eventsToShow.isEmpty) {
                        return Center(child: Text("Aucun événement à afficher"));
                      }

                      return ListView.builder(
                        itemCount: eventsToShow.length,
                        itemBuilder: (context, index) {
                          var event = eventsToShow[index];
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.15), // Ajoutez des marges de 10% de chaque côté
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                  context,
                                  PageRouteBuilder(
                                    transitionDuration: const Duration(milliseconds: 100),
                                    pageBuilder: (_, __, ___) => DetailedEvent(event: event),
                                    transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child),
                                  ),
                                );
                              },
                              child: Stack(
                                clipBehavior: Clip.none,
                                children: [
                                Positioned(
                                top: -15, // Ajustez cette valeur pour positionner le texte
                                right: 25, // Ajustez cette valeur pour positionner le texte
                                child: Text(
                                  utf8.decode(event['statut'].codeUnits),
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: Color(0xFF3B4549),
                                  ),
                                ),
                              ),
                              Container(
                                width: double.infinity, // Utilisez toute la largeur disponible
                                height: 193,
                                margin: const EdgeInsets.only(bottom: 26), // Ajoutez une marge en bas pour espacer les cartes
                                child: Card(
                                  elevation: 4,
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                  child: Stack(
                                    clipBehavior: Clip.none,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.circular(10),
                                              child: Container(
                                                width: double.infinity, // Utilisez toute la largeur disponible
                                                height: 66,
                                                color: const Color(0xFFE0E0E0),
                                                child: () {
                                                  try {
                                                    if (event['image'] != null && event['image'].trim().isNotEmpty) {
                                                      return Image.memory(base64Decode(event['image']), fit: BoxFit.cover);
                                                    }
                                                  } catch (e) {
                                                    print('Erreur de décodage de l\'image: $e');
                                                  }
                                                  return Image.asset("assets/logo_background.png", fit: BoxFit.cover);
                                                }(),
                                              ),
                                            ),
                                            const SizedBox(height: 8),
                                            Text(
                                              utf8.decode((event['label'] ?? 'Absent').runes.toList()),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                                            ),
                                            const SizedBox(height: 8),
                                            Text(EventWidget.getFormattedDateTimeString(event, context)),
                                            Text(utf8.decode((event['lieu']['adresse'] ?? 'Absent').runes.toList())),
                                          ],
                                        ),
                                      ),
                                      Positioned(
                                        top: -15, // Déplace l'image de 5px vers le haut
                                        right: -15, // Déplace l'image de 5px vers la droite
                                        child: Image.asset(
                                                () {
                                              String statut = utf8.decode(event['statut'].codeUnits);
                                              print('Statut de l\'événement: $statut'); // Ajoutez cette ligne pour déboguer
                                              switch (statut) {
                                                case 'Accepté':
                                                  return 'assets/validation.png';
                                                case 'Refusé':
                                                  return 'assets/refus.png';
                                                default:
                                                  return 'assets/attente.png';
                                              }
                                            }(),
                                            width: 30,
                                            height: 30
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    }
                  },
                ),
              ),
              const SizedBox(height: 26.0),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavBar(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),

      floatingActionButton: Container(
        width: 40, // Largeur personnalisée
        height: 40, // Hauteur personnalisée
        child: FloatingActionButton(
          backgroundColor: const Color(0xFF4C780E),
          onPressed: () {
            Get.to(() => const CreateEvent(),
            transition: Transition.fadeIn,
            duration: const Duration(milliseconds: 500),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}

import 'dart:async';
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../../controller/auth_controller.dart';
import '../../controller/evenements_controller.dart';
import '../../widgets/AnimatedToggle.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../../widgets/custom_dialog.dart';
import '../../widgets/event_widget.dart';
import '../contributions/contributions_view.dart';
import '../event/create_event.dart';
import '../event/detailed_event_register.dart';
import '../home/home_screen.dart';
import '../profile/profile_view.dart';
import 'package:http/http.dart' as http;

class FavouriteScreen extends StatefulWidget {

  const FavouriteScreen({Key? key}) : super(key: key);

  @override
  State<FavouriteScreen> createState() => _FavouriteScreenState();

}

class _FavouriteScreenState extends State<FavouriteScreen> {

  late AuthController _authController;
  int _selectedIndex = 1;
  bool _showUpcomingEvents = true;
  List<bool> _isSelected = [true, false];
  int _toggleIndex = 0;
  final StreamController<List<dynamic>> _streamController = StreamController<List<dynamic>>();

  List<dynamic> filterUpcomingEvents(List<dynamic> events) {
    var now = DateTime.now();
    return events.where((event) {
      var eventDate = DateFormat("dd/MM/yyyy").parse(event['date_debut']);
      return eventDate.isAfter(now);
    }).toList();
  }

  List<dynamic> filterPastEvents(List<dynamic> events) {
    var now = DateTime.now();
    return events.where((event) {
      var eventEndDate = DateFormat("dd/MM/yyyy").parse(event['date_fin']);
      return eventEndDate.isBefore(now);
    }).toList();
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
            () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
            () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
            () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
            () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  void updateFavoris() async {
    var favoris = await getFavorisById();
    _streamController.add(favoris);
  }

  Future<List<dynamic>> getFavorisById() async {
    var now = DateTime.now();
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;

    final String url = "http://amie.labinno-mtech.fr/api/evenement/getfavorisbyidutilisateur/$idUtilisateur";
    AuthController authController = Get.find<AuthController>();

    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse;
    } else {
      throw Exception('Erreur de chargement des inscriptions aux événements');
    }
  }

  Future<void> deleteFavoris(int idEvenement) async {
    AuthController _authController = Get.find<AuthController>();
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    print(idEvenement);

    var response = await http.delete(
      Uri.parse('http://amie.labinno-mtech.fr/api/evenement/deletefavoris/$idEvenement/$idUtilisateur'),
      headers: {
        'Authorization': _authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    print('Response body: ${response.body}');
    print('Response status: ${response.statusCode}');

    if (response.statusCode != 200) {
      throw Exception('Erreur lors de la suppression de l\'événement des favoris');
    }

    updateFavoris();

  }

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
    getFavorisById();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () => Get.back(),
                  ),
                  const Text(
                    'Mes favoris',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Inter',
                    ),
                  ),
                ],
              ),
              const SizedBox(height: 20),

              LayoutBuilder(
                builder: (BuildContext context, BoxConstraints constraints) {
                  double toggleButtonWidth = constraints.maxWidth * 0.7; // 70% de la largeur totale

                  return Center(
                    child: AnimatedToggle(
                      onToggle: (bool isUpcomingSelected) {
                        setState(() {
                          _showUpcomingEvents = isUpcomingSelected;
                        });
                      },
                    ),
                  );
                },
              ),

              Expanded(
                child: FutureBuilder<List<dynamic>>(
                  future: getFavorisById(),
                  builder: (context, snapshot) {
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(child: CircularProgressIndicator());
                    } else if (snapshot.hasError) {
                      return Text("Erreur: ${snapshot.error}");
                    } else {
                      var allEvents = snapshot.data!;
                      var upcomingEvents = filterUpcomingEvents(allEvents);
                      var pastEvents = filterPastEvents(allEvents);
                      var eventsToShow = _showUpcomingEvents ? upcomingEvents : pastEvents;

                      return ListView.builder(
                        padding: const EdgeInsets.all(8.0),
                        itemCount: eventsToShow.isEmpty ? 1 : eventsToShow.length,
                        itemBuilder: (context, index) {
                          if (eventsToShow.isEmpty) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1), // Ajoutez des marges de 10% de chaque côté
                              child: Card(
                                elevation: 4,
                                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Stack(
                                        children: [
                                          ClipRRect(
                                            borderRadius: BorderRadius.circular(10),
                                            child: Container(
                                              width: double.infinity, // Utilisez toute la largeur disponible
                                              height: 193 * 0.7, // environ 70% de la hauteur totale de la Card
                                              decoration: const BoxDecoration(color: Color(0xFFE0E0E0)),
                                              child: Image.asset(
                                                "assets/aucun_evenement.png",
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      const SizedBox(height: 8),
                                      const Text(
                                        'Ajoutez des événements à vos favoris pour les retrouver ici !',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                                        maxLines: null, // Autoriser un nombre illimité de lignes
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            );
                          } else {
                            var event = eventsToShow[index];
                            return Padding(
                              padding: EdgeInsets.symmetric(
                                  horizontal: MediaQuery.of(context).size.width * 0.1,
                                  vertical: 16.0),
                              child: Stack(
                                clipBehavior: Clip.none, // Ajoutez cette ligne
                                children: [
                                  InkWell(
                                    onTap: () {
                                      Navigator.push(
                                        context,
                                        PageRouteBuilder(
                                          transitionDuration: const Duration(milliseconds: 100),
                                          pageBuilder: (_, __, ___) => DetailedEventRegister(event: event),
                                          transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child),
                                        ),
                                      );
                                    },
                                    child: Card(
                                      elevation: 4,
                                      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.circular(10),
                                              child: Container(
                                                width: double.infinity,
                                                height: 66,
                                                color: const Color(0xFFE0E0E0),
                                                child: () {
                                                  try {
                                                    if (event['image'] != null && event['image'].trim().isNotEmpty) {
                                                      return Image.memory(base64Decode(event['image']), fit: BoxFit.cover);
                                                    }
                                                  } catch (e) {
                                                    print('Erreur de décodage de l\'image: $e');
                                                  }
                                                  return Image.asset("assets/logo_background.png", fit: BoxFit.cover);
                                                }(),
                                              ),
                                            ),
                                            const SizedBox(height: 8),
                                            Text(
                                              utf8.decode((event['label'] ?? 'Absent').runes.toList()),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                                            ),
                                            const SizedBox(height: 10),
                                            Text(EventWidget.getFormattedDateTimeString(event, context)),
                                            Text(utf8.decode((event['lieu']['adresse'] ?? 'Absent').runes.toList())),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),

                                  Positioned(
                                    top: -20, // ajustez ces valeurs comme vous le souhaitez
                                    right: -20, // ajustez ces valeurs comme vous le souhaitez
                                    child: GestureDetector(
                                      onTap: () async {
                                        var customDialog = CustomDialog(context);
                                        bool? confirmationResult = await customDialog.displayConfirmationDialog(
                                          'Confirmation', 'Voulez-vous vraiment supprimer cet événement des favoris ?',
                                        );
                                        if (confirmationResult == true) {
                                          await deleteFavoris(event['idEvenement']);
                                          updateFavoris();
                                          setState(() {}); // Met à jour l'état pour rafraîchir l'interface utilisateur
                                        }
                                      },
                                      child: const Material(
                                        elevation: 40.0, // ajustez cette valeur pour changer l'effet de surélévation
                                        color: Colors.transparent, // assurez-vous que le widget Material est transparent
                                        child: Image(
                                          image: AssetImage('assets/icone_supprimer.png'),
                                          width: 50, // ajustez cette valeur comme vous le souhaitez
                                          height: 50, // ajustez cette valeur comme vous le souhaitez
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            );
                          }
                        },
                      );
                    }
                  },
                ),
              ),
              const SizedBox(height: 26.0),
            ],
          ),
        ),
      ),

      bottomNavigationBar: BottomNavBar(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      floatingActionButton: Container(
        width: 40, // Largeur personnalisée
        height: 40, // Hauteur personnalisée
        child: FloatingActionButton(
          backgroundColor: const Color(0xFF4C780E),
          onPressed: () {
            Get.to(() => const CreateEvent(),
              transition: Transition.fadeIn,
              duration: const Duration(milliseconds: 500),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import '../../controller/auth_controller.dart';
import '../../controller/evenements_controller.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../contributions/contributions_view.dart';
import '../event/create_event.dart';
import '../event/detailed_event.dart';
import '../event/detailed_event_inscription.dart';
import '../event/detailed_event_register.dart';
import '../favourites/favourite_view.dart';
import '../profile/profile_view.dart';
import 'package:flutter/rendering.dart' as rendering;
import '../../widgets/event_widget.dart';
import 'package:http/http.dart' as http;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  late AuthController _authController;
  late EvenementsController _evenementsController;
  int _selectedIndex = 0;
  final ScrollController _filterScrollController = ScrollController();
  // Ajout d'une nouvelle liste pour conserver tous les événements
  List<dynamic> _events = [];
  List<dynamic> upcomingEvents = [];
  List<dynamic> pastEvents = [];
  List<dynamic> registeredEvents = [];
  List<String> selectedLabels = [];
  Future<List<dynamic>>? _fetchUserRegisteredEvents;
  Future<List<dynamic>>? _fetchEventsFuture;
  Future<List<dynamic>>? _fetchEventsPast;

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
            () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
            () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
            () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
            () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  List<String> _selectedFilter = [];

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
    _evenementsController = Get.put(EvenementsController());
    fetchEvents(); // Appel de la fonction fetchEvents dans initState
    _fetchUserRegisteredEvents = fetchUserRegisteredEvents(); // Ajout de l'appel à la nouvelle fonction
    _fetchEventsFuture = fetchEvents(); // Enregistre le Future renvoyé par fetchEvents()
    _fetchEventsPast = fetchEvents(); // Enregistre le past renvoyé par fetchEvents()
    upcomingEvents = filterUpcomingEvents(_events);
    pastEvents = filterPastEvents(_events);
    selectedLabels = ['Tout']; // Initialiser selectedLabels avec 'Tout'
    _selectedFilter = ['Tout']; // Initialiser _selectedFilter avec 'Tout'
  }

  Future<List<dynamic>> fetchEvents() async {
    _events = await _evenementsController.getAllEvenements(_authController.accessToken.value);
    return _events;
  }

  Future<double> getMaxLabelWidth(List<String> labels, TextStyle style) async {
    final TextPainter textPainter = TextPainter(
      textDirection: rendering.TextDirection.ltr,
    );

    double maxWidth = 0;
    for (String label in labels) {
      textPainter.text = TextSpan(text: label, style: style);
      textPainter.layout();
      if (textPainter.width > maxWidth) {
        maxWidth = textPainter.width;
      }
    }
    return maxWidth;
  }

  void checkAllFiltersDisabled() {
    if (selectedLabels.isEmpty) {
      selectedLabels.add('Tout');
    }
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _selectedFilter = ['Tout'];
  }

  List<dynamic> filterUpcomingEvents(List<dynamic> events) {
    var now = DateTime.now();
    return events.where((event) {
      print('Event: $event'); // Ajoutez cette ligne pour imprimer chaque événement
      if (event['date_fin'] == null || event['date_fin'].trim().isEmpty) {
        return false; // Ignore cet événement si 'date_fin' est null ou une chaîne vide
      }
      try {
        var eventEndDate = DateFormat("dd/MM/yyyy").parse(event['date_fin']);
        return eventEndDate.isAfter(now);
      } catch (FormatException) {
        print('Invalid date format for event ${event['name']}');
        return false; // Ignore cet événement si 'date_fin' n'est pas une date valide
      }
    }).toList();
  }

  List<dynamic> filterPastEvents(List<dynamic> events) {

    var now = DateTime.now();
    return events.where((event) {
      print('Event: $event'); // Add this line to print each event
      if (event['date_fin'] == null || event['date_fin'].trim().isEmpty) {
        return false; // Ignore this event if 'date_fin' is null or an empty string
      }
      try {
        var eventEndDate = DateFormat("dd/MM/yyyy").parse(event['date_fin']);
        return eventEndDate.isBefore(now);
      } catch (FormatException) {
        print('Invalid date format for event ${event['name']}');
        return false; // Ignore this event if 'date_fin' is not a valid date
      }
    }).toList();
  }

  List<dynamic> filterEventsByLabels(List<dynamic> events, List<String> labels) {
    // Si "Tout" est sélectionné, retournez tous les événements.
    if (labels.contains('Tout')) {
      return events;
    } else {
      return events.where((event) {
        // Pour chaque événement, récupérez tous les labels de typologie.
        if (event['typologieEvenements'] is List) {
          List<String> eventLabels = event['typologieEvenements']
              .map((typologieEvenement) => typologieEvenement['label'])
              .whereType<String>()
              .toList();
          // Vérifiez si tous les labels sélectionnés sont dans la liste des labels de l'événement.
          return labels.every((label) => eventLabels.contains(label));
        }
        return false;
      }).toList();
    }
  }

  List<String> getAllLabels(List<dynamic> events) {
    Set<String> labels = {'Tout'};
    for (var event in events) {
      if (event['typologieEvenements'] is List) {
        for (var typologieEvenement in event['typologieEvenements']) {
          labels.add(typologieEvenement['label'] ?? 'Absent');
        }
      }
    }
    return labels.toList();
  }

  void onLabelSelected(String label) {
    setState(() {
      if (label == 'Tout') {
        selectedLabels.clear();
        selectedLabels.add('Tout');
      } else {
        if (selectedLabels.contains('Tout')) {
          selectedLabels.remove('Tout');
        }
        if (selectedLabels.contains(label)) {
          selectedLabels.remove(label);
        } else {
          selectedLabels.add(label);
        }

        // Si aucun label n'est sélectionné, sélectionnez 'Tout' par défaut
        if (selectedLabels.isEmpty) {
          selectedLabels.add('Tout');
        }
      }
    });
  }
  /*
    Cette méthode effectue une nouvelle demande à l'API pour chaque événement, ce qui pourrait être lent si beaucoup d'événements.
   Si  API offre un moyen d'obtenir plusieurs événements en une seule requête en utilisant leurs ID, cela serait plus efficace.
   */
  Future<List<dynamic>> fetchUserRegisteredEvents() async {
    var now = DateTime.now();
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    final String url = "http://amie.labinno-mtech.fr/api/evenement/getinscriptions/$idUtilisateur";

    AuthController authController = Get.find<AuthController>();

    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      List jsonResponse = json.decode(response.body);
      return jsonResponse;
    } else {
      throw Exception('Erreur de chargement des inscriptions aux événements');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Builder(
        builder: (BuildContext context) {
          return SafeArea(
              child: Container(
              decoration: const BoxDecoration(
              image: DecorationImage(
              image: AssetImage("assets/background.png"),
          fit: BoxFit.cover,
          ),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 20),
          height: double.infinity,
          width: double.infinity,
          child: RefreshIndicator(
          onRefresh: () async {
          setState(() {
          _fetchEventsFuture = fetchEvents(); // Re-fetch events
          _fetchUserRegisteredEvents = fetchUserRegisteredEvents(); // Re-fetch user registered events
          });
          },
          child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(height: Get.height * 0.02),
                    const SizedBox(height: 18.0),
                    Obx(() {
                      return Text(
                        'Bienvenue, ${_authController.loggedInUser.value.prenom}',
                        style: const TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w300,
                          fontFamily: 'Inter',
                        ),
                      );
                    }),
                    const SizedBox(height: 18.0),

                    FutureBuilder<List<dynamic>>(
                      future: _fetchEventsFuture,
                      builder: (context, snapshot) {if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (snapshot.hasError) {
                        return Text("Erreur: ${snapshot.error}");
                      } else {
                        List<String> labels = getAllLabels(snapshot.data!);
                        List<dynamic> filteredEvents = filterEventsByLabels(snapshot.data!, _selectedFilter);
                        upcomingEvents = filterUpcomingEvents(filteredEvents);
                        pastEvents = filterPastEvents(filteredEvents);

                        return Column(
                          children: [
                            FutureBuilder<double>(
                              future: getMaxLabelWidth(labels,
                                  Theme.of(context).textTheme.bodyLarge!),
                              builder: (context, snapshotMaxWidth) {
                                if (!snapshotMaxWidth.hasData) {
                                  return const CircularProgressIndicator();
                                }
                                final maxLabelWidth = snapshotMaxWidth.data!;

                                return SizedBox(
                                  height: 33,
                                  child: ListView.builder(
                                    controller: _filterScrollController,
                                    scrollDirection: Axis.horizontal,
                                    itemCount: labels.length,
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          setState(() {
                                            if (labels[index] == 'Tout') {
                                              _selectedFilter = ['Tout'];
                                              // Filtrage par label
                                              filteredEvents = filterEventsByLabels(snapshot.data!, _selectedFilter);
                                              // Filtrage par statut
                                              upcomingEvents = filterUpcomingEvents(
                                                  filteredEvents);
                                              pastEvents = filterPastEvents(
                                                  filteredEvents);
                                            } else if (_selectedFilter.contains('Tout')) {
                                              // Si "Tout" est déjà sélectionné et que l'utilisateur clique sur un autre label, "Tout" est supprimé et le nouveau label est ajouté
                                              _selectedFilter = [
                                                labels[index]
                                              ];
                                            } else if (_selectedFilter.contains(labels[index])) {
                                              _selectedFilter.remove(labels[index]);
                                            } else {
                                              _selectedFilter.add(labels[index]);
                                            }

                                            // Vérifier si _selectedFilter est vide après avoir retiré un label. Si c'est le cas, ajouter 'Tout' à _selectedFilter
                                            if (_selectedFilter.isEmpty) {
                                              _selectedFilter.add('Tout');
                                            }

                                            if (_selectedFilter.contains('Tout')) {
                                              // Si "Tout" est sélectionné, tous les événements sont inclus
                                              filteredEvents = snapshot.data!;
                                              upcomingEvents = snapshot.data!;
                                              pastEvents = snapshot.data!;
                                            } else {
                                              filteredEvents = filterEventsByLabels(snapshot.data!, _selectedFilter);
                                              upcomingEvents = filterUpcomingEvents(snapshot.data!);
                                              pastEvents = filterPastEvents(snapshot.data!);
                                            }
                                          });
                                          print('Filtre sélectionné: $_selectedFilter');
                                        },
                                        child: Container(
                                          width: maxLabelWidth + 16,
                                          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
                                          margin: const EdgeInsets.symmetric(
                                              horizontal: 4),
                                          decoration: BoxDecoration(
                                            color: _selectedFilter.contains(labels[index]) ? const Color(0xFF6AA517) : Colors.grey[300],
                                            borderRadius: BorderRadius.circular(40),
                                          ),
                                          child: Center(
                                            child: Text(
                                              labels[index],
                                            ),
                                          ),
                                        ),
                                      );
                                    },
                                  ),
                                );
                              },
                            ),
                            const SizedBox(height: 18.0),

                            const Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Mes événements',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Inter',
                                ),
                              ),
                            ),
                            const SizedBox(height: 4.0),

                            FutureBuilder<List<dynamic>>(
                              future: _fetchUserRegisteredEvents,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return const Center(child: CircularProgressIndicator());
                                } else if (snapshot.hasError) {
                                  return Text("Erreur: ${snapshot.error}");
                                } else {
                                  var allEvents = snapshot.data!;
                                  List<dynamic> registeredEvents = filterEventsByLabels(allEvents, _selectedFilter);
                                  registeredEvents = registeredEvents.toList(); // Ajout de la conversion en liste

                                  return SizedBox(
                                    height: 200,
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        children: registeredEvents.isEmpty ? [
                                          const SizedBox(width: 10),
                                          Card(
                                            elevation: 4,
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Stack(
                                                    children: [
                                                      ClipRRect(borderRadius: BorderRadius.circular(10),
                                                        child: Container(
                                                          width: 190,
                                                          height: 193 * 0.7, // environ 70% de la hauteur totale de la Card
                                                          decoration: const BoxDecoration(color: Color(0xFFE0E0E0)),
                                                          child: Image.asset("assets/aucun_evenement.png", fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(height: 16),
                                                  const SizedBox(
                                                    width: 190,
                                                    child: Text('Vous n\'avez aucune inscription', textAlign: TextAlign.center,
                                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ] : [
                                          const SizedBox(width: 10),
                                          for (var event in registeredEvents)
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                    transitionDuration: const Duration(milliseconds: 100),
                                                    pageBuilder: (_, __, ___) => DetailedEventRegister(event: event),
                                                    transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                width: 218,
                                                height: 193,
                                                margin: const EdgeInsets.only(right: 10),
                                                child: Card(
                                                  elevation: 4,
                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        ClipRRect(
                                                          borderRadius: BorderRadius.circular(10),
                                                          child: Container(
                                                            width: 190,
                                                            height: 66,
                                                            color: const Color(0xFFE0E0E0),
                                                            child: () {
                                                              try {
                                                                if (event['image'] != null && event['image'].trim().isNotEmpty) {
                                                                  return Image.memory(base64Decode(event['image']), fit: BoxFit.cover);
                                                                }
                                                              } catch (e) {
                                                                print('Erreur de décodage de l\'image: $e');
                                                              }
                                                              return Image.asset("assets/logo_background.png", fit: BoxFit.cover);
                                                            }(),
                                                          ),
                                                        ),
                                                        const SizedBox(height: 8),
                                                        Text(
                                                          utf8.decode((event['label'] ?? 'Absent').runes.toList()),
                                                          maxLines: 1,
                                                          overflow: TextOverflow.ellipsis,
                                                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                                                        ),
                                                        const SizedBox(height: 8),
                                                        Text(EventWidget.getFormattedDateTimeString(event, context)),
                                                        Text(utf8.decode((event['lieu']['adresse'] ?? 'Absent').runes.toList())),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                            const SizedBox(height: 8.0),

                            const Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Événements à venir',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Inter',
                                ),
                              ),
                            ),
                            const SizedBox(height: 4.0),

                            FutureBuilder<List<dynamic>>(
                              future: _fetchEventsFuture,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return const Center(child: CircularProgressIndicator());
                                } else if (snapshot.hasError) {
                                  return Text("Erreur: ${snapshot.error}");
                                } else {
                                  var allEvents = snapshot.data!;
                                  List<dynamic> filteredEvents = filterEventsByLabels(allEvents, _selectedFilter);
                                  upcomingEvents = filterUpcomingEvents(filteredEvents);

                                  return SizedBox(
                                    height: 200,
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        children: upcomingEvents.isEmpty ? [
                                          const SizedBox(width: 10),
                                          Card(
                                            elevation: 4,
                                            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18),
                                            ),
                                            child: Padding(
                                              padding: const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Stack(
                                                    children: [
                                                      ClipRRect(borderRadius: BorderRadius.circular(10),
                                                        child: Container(
                                                          width: 190,
                                                          height: 193 * 0.7, // environ 70% de la hauteur totale de la Card
                                                          decoration: const BoxDecoration(color: Color(0xFFE0E0E0)),
                                                          child: Image.asset("assets/aucun_evenement.png", fit: BoxFit.cover),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(height: 16),
                                                  const SizedBox(
                                                    width: 190,
                                                    child: Text('Nous organisons vos événements', textAlign: TextAlign.center,
                                                      style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ] : [
                                          const SizedBox(width: 10),
                                          for (var event in upcomingEvents)
                                            GestureDetector(
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  PageRouteBuilder(
                                                    transitionDuration: const Duration(milliseconds: 100),
                                                    pageBuilder: (_, __, ___) => DetailedEventInscription(event: event),
                                                    transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child),
                                                  ),
                                                );
                                              },
                                              child: Container(
                                                width: 218,
                                                height: 193,
                                                margin: const EdgeInsets.only(right: 10),
                                                child: Card(
                                                  elevation: 4,
                                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                                                  child: Padding(
                                                    padding: const EdgeInsets.all(8.0),
                                                    child: Column(crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        ClipRRect(
                                                          borderRadius: BorderRadius.circular(10),
                                                          child: Container(
                                                            width: 190,
                                                            height: 66,
                                                            color: const Color(0xFFE0E0E0),
                                                            child: () {
                                                              try {
                                                                if (event['image'] != null && event['image'].trim().isNotEmpty) {
                                                                  return Image.memory(base64Decode(event['image']), fit: BoxFit.cover);
                                                                }
                                                              } catch (e) {
                                                                print('Erreur de décodage de l\'image: $e');
                                                              }
                                                              return Image.asset("assets/logo_background.png", fit: BoxFit.cover);
                                                            }(),
                                                          ),
                                                        ),
                                                        const SizedBox(height: 8),
                                                        Text(event['label'] ?? 'Absent', maxLines: 1, overflow: TextOverflow.ellipsis,
                                                          style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                                                        ),
                                                        const SizedBox(height: 8),
                                                        Text(EventWidget.getFormattedDateTimeString(event, context)),
                                                        Text('${event['lieu']['adresse'] ?? 'Absent'}'),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                        ],
                                      ),
                                    ),
                                  );
                                }
                              },
                            ),
                            const SizedBox(height: 8.0),

                            const Align(
                              alignment: Alignment.centerLeft,
                              child: Text(
                                'Événements passés',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w500,
                                  fontFamily: 'Inter',
                                ),
                              ),
                            ),
                            const SizedBox(height: 4.0),

                            FutureBuilder<List<dynamic>>(
                              future: _fetchEventsPast,
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return const Center(child: CircularProgressIndicator());
                                } else if (snapshot.hasError) {
                                  return Text("Erreur: ${snapshot.error}");
                                } else {
                                  List<dynamic> filteredEvents = filterEventsByLabels(snapshot.data!, _selectedFilter);
                                  pastEvents = filterPastEvents(filteredEvents);

                                  return SizedBox(
                                    height: 200,
                                    child: SingleChildScrollView(
                                      scrollDirection: Axis.horizontal,
                                      child: Row(
                                        children: pastEvents.isEmpty ? [
                                          const SizedBox(width: 10),
                                          Card(
                                            elevation: 4,
                                            shape:
                                            RoundedRectangleBorder(
                                              borderRadius: BorderRadius.circular(18),
                                            ),
                                            child: Padding(
                                              padding:
                                              const EdgeInsets.all(8.0),
                                              child: Column(
                                                crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                                children: [
                                                  Stack(
                                                    children: [
                                                      ClipRRect(
                                                        borderRadius:
                                                        BorderRadius.circular(10),
                                                        child:
                                                        Container(
                                                          width: 190,
                                                          height:
                                                          193 * 0.7,
                                                          // environ 70% de la hauteur totale de la Card
                                                          decoration:
                                                          const BoxDecoration(color: Color(0xFFE0E0E0),
                                                          ),
                                                          child: Image.asset("assets/aucun_evenement.png", fit: BoxFit.cover,
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                  const SizedBox(height: 16),
                                                  const SizedBox(
                                                    width: 190,
                                                    // la même largeur que celle définie pour le Container de l'image
                                                    child: Text('Nous organisons vos événements',
                                                      textAlign:
                                                      TextAlign.center,
                                                      style: TextStyle(fontSize: 12,
                                                        fontWeight: FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ] : [
                                          const SizedBox(width: 10),
                                          for (var event in pastEvents)
                                            GestureDetector(
                                            onTap: () {
                                              Navigator.push(
                                                context,
                                                PageRouteBuilder(
                                                  transitionDuration: const Duration(milliseconds: 100),
                                                  pageBuilder: (_, __, ___) => DetailedEvent(event: event),
                                                  transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child),
                                                ),
                                              );
                                            },
                                  child :Container(
                                    width: 218,
                                    height: 193,
                                    margin:
                                    const EdgeInsets.only(right: 10),
                                    child: Card(
                                      elevation: 4,
                                      shape:
                                      RoundedRectangleBorder(borderRadius: BorderRadius.circular(18),
                                      ),
                                      child: Padding(
                                        padding:
                                        const EdgeInsets.all(8.0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            ClipRRect(
                                              borderRadius: BorderRadius.circular(10),
                                              child:
                                              Container(
                                                width: 190,
                                                height: 66,
                                                color: const Color(0xFFE0E0E0),
                                                child: () {
                                                  try {
                                                    if (event['image'] != null && event['image'].trim().isNotEmpty) {
                                                      return Image.memory(base64Decode(event['image']), fit: BoxFit.cover,
                                                      );
                                                    }
                                                  } catch (e) {
                                                    print('Erreur de décodage de l\'image: $e');
                                                  }
                                                  return Image.asset("assets/logo_background.png", fit: BoxFit.cover);
                                                }(),
                                              ),
                                            ),
                                            const SizedBox(height: 8),

                                            Text(event['label'] ?? 'Absent', maxLines: 1, overflow: TextOverflow.ellipsis,
                                              style: const TextStyle(fontWeight: FontWeight.bold,
                                                fontSize: 16,
                                              ),
                                            ),
                                            const SizedBox(height: 8),
                                            Text(EventWidget.getFormattedDateTimeString(event, context)),
                                            Text('${event['lieu']['adresse'] ?? 'Absent'}',
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                        },
                      ),
                      const SizedBox(height: 26.0),
                        ],
                      );
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  },
),
      bottomNavigationBar: BottomNavBar(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
      ),
      floatingActionButton: SizedBox(
        width: 40,
        height: 40,
        child: FloatingActionButton(
          backgroundColor: const Color(0xFF4C780E),
          onPressed: () {
            Get.to(
                  () => const CreateEvent(),
              transition: Transition.fadeIn,
              duration: const Duration(milliseconds: 500),
            );
          },
          child: const Icon(Icons.add),
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}


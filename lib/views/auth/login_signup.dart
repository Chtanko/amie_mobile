import 'dart:ui';
import 'package:ems/controller/auth_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:ems/utils/app_color.dart';
import 'package:ems/widgets/custom_widgets.dart';
import '../create_profile.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
    State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {

  GlobalKey<FormState> formKey = GlobalKey<FormState>();

  TextEditingController nameController = TextEditingController();
  TextEditingController surnameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  int selectedRadio = 0;
  TextEditingController forgetEmailController = TextEditingController();

  void setSelectedRadio(int val) {
    setState(() {
      selectedRadio = val;
    });
  }

  bool isSignUp = false;

  late AuthController authController;

  @override
  void initState() {
    super.initState();
    authController = Get.put(AuthController());
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        body: Stack(
          children: [
          // Ajout de l'image en arrière-plan
          Image.asset(
          'assets/background.png',
          fit: BoxFit.cover,
          width: double.infinity,
          height: double.infinity,
        ),

        SingleChildScrollView(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: Get.width * 0.05),
            child: Column(
              children: [
                SizedBox( height: Get.height * 0.08,
                ),
                isSignUp ? myText(
                  text: 'Inscrivez-vous',
                  style: const TextStyle(
                    fontSize: 23,
                    fontWeight: FontWeight.w600,
                  ),
                )
                    : myText(
                  text: 'Connectez-vous',
                  style: GoogleFonts.poppins(
                    fontSize: 23,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(
                  height: Get.height * 0.03,
                ),
                isSignUp
                    ? Container(
                  child: myText(
                    text:
                    'Bienvenue !',
                    style: GoogleFonts.roboto(
                      letterSpacing: 0,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                )
                : Container(
                  child: myText(
                    text:
                    'Contents de vous revoir !',
                    style: GoogleFonts.roboto(
                      letterSpacing: 0,
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(
                  height: Get.height * 0.03,
                ),

                Container(
                  width: Get.width, // Modification pour prendre toute la largeur de l'écran
                  child: TabBar(
                    isScrollable: false, // Ajout de cette ligne pour garantir que les onglets occupent toute la largeur
                    labelPadding: EdgeInsets.all(Get.height * 0.01),
                    unselectedLabelColor: Colors.grey,
                    labelColor: Colors.black,
                    indicatorColor: Colors.black,
                    onTap: (v) {
                      setState(() {
                        isSignUp = !isSignUp;
                      });
                    },
                    tabs: [
                      myText(
                        text: 'Connexion',
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w500,
                            color: AppColors.black),
                      ),
                      myText(
                        text: 'Inscription',
                        style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w500,
                          color: AppColors.black,
                        ),
                      ),
                    ],
                  ),
                ),

                SizedBox(
                  height: Get.height * 0.04,
                ),
                Container(
                  width: Get.width,
                  height: Get.height * 0.6,
                  child: Form(
                    key: formKey,
                    child: TabBarView(
                      physics: const NeverScrollableScrollPhysics(),
                      children: [
                        LoginWidget(),
                        SignUpWidget(),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget LoginWidget(){
    return SingleChildScrollView(
      child: Column(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              myTextField(
                  bool: false,
                  icon: 'assets/mail.png',
                  text: 'Adresse email',
                  validator: (String input){
                    if(input.isEmpty){
                      Get.snackbar(
                          'Attention',
                          'Adresse mail MGEN obligatoire',
                          colorText: Colors.white,
                          backgroundColor: const Color(0xFF6AA517));
                      return '';
                    }

                    if(!input.contains('@mgen.fr')){
                      Get.snackbar(
                          'Attention',
                          'Adresse mail inconnue',
                          colorText: Colors.white,
                          backgroundColor: const Color(0xFF6AA517));
                      return '';
                    }
                  },
                  controller: emailController
              ),

              SizedBox(
                height: Get.height * 0.02,
              ),
              myTextField(
                  bool: true,
                  icon: 'assets/lock.png',
                  text: 'Mot de passe',
                  validator: (String input){
                    if(input.isEmpty){
                      Get.snackbar('Attention',
                          'Mot de passe obligatoire',
                          colorText: Colors.white, backgroundColor: const Color(0xFF6AA517));
                      return '';
                    }
                    if(input.length <3){
                      Get.snackbar('Attention',
                          'Mot de passe doit avoir au moins 3 charactères',
                          colorText: Colors.white,backgroundColor: const Color(0xFF6AA517));
                      return '';
                    }
                  },
                  controller: passwordController
              ),

              InkWell(
                onTap: () {
                  Get.defaultDialog(
                      title: 'Réinitialiser le mot de passe ',
                      content: SizedBox(
                        width: Get.width,
                        child: Column(
                          children: [
                            myTextField(
                                bool: false,
                                icon: 'assets/mail.png',
                                text: 'Adresse email MGEN',
                              controller: forgetEmailController
                            ),
                            const SizedBox(
                              height: 8,
                            ),

                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.1),
                          child: SizedBox(
                            width: double.infinity,
                            child: ElevatedButton(
                              onPressed: () {
                                authController.forgetPassword(forgetEmailController.text.trim());
                              },
                              style: ButtonStyle(
                                backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF6AA517)),
                                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              ),
                              child: const Text(
                                "Envoyer",
                                style: TextStyle(
                                  fontSize: 18,
                                  color: Colors.white,
                                  fontWeight: FontWeight.w500,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                  );
                },
                child: Container(
                  margin: EdgeInsets.only(
                    top: Get.height * 0.02,
                  ),
                  child: myText(
                      text: 'Mot de passe oublié',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: AppColors.black,
                      )),
                ),
              ),
            ],
          ),

          Obx(
              () => authController.isLoading.value ? const Center(
                child: CircularProgressIndicator(
                ),
              ) : Container(
            height: 58,
            margin: EdgeInsets.symmetric(
              vertical: Get.height * 0.04,
            ),
            width: Get.width,
            child: ElevatedButton(
              onPressed: () {
                if (!formKey.currentState!.validate()) {
                  return;
                }
                authController.login(
                  email: emailController.text.trim(),
                  password: passwordController.text.trim(),
                );
              },
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF6AA517)),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(15), // Définition du borderRadius à 15
                  ),
                ),
              ),
              child: const Text( "SE CONNECTER",
                style: TextStyle(
                    fontSize: 18,
                    color: Colors.white,
                    fontWeight: FontWeight.w500
                ),
              ),
            ),
          ),
        ),

          SizedBox(
            height: Get.height * 0.02,
          ),
          SizedBox(
            height: Get.height * 0.01,
          ),

        ],
      ),
    );
  }

  Widget SignUpWidget(){

    return SingleChildScrollView(

        child: Column(
          children: [
            myTextField(
                bool: false,
                icon: 'assets/profil.png',
                text: 'Prénom',
                validator: (String input){
                  if(input.isEmpty){
                    Get.snackbar(
                        'Attention', 'Votre prénom est obligatoire',
                        colorText: Colors.white,backgroundColor: const Color(0xFF6AA517));
                    return '';
                  }
                },
                controller: nameController
            ),

            SizedBox(
              height: Get.height * 0.02,
            ),

            myTextField(
                bool: false,
                icon: 'assets/profil.png',
                text: 'Nom',
                validator: (String input){
                  if(input.isEmpty){
                    Get.snackbar('Attention',
                        'Votre nom est obligatoire',colorText: Colors.white,backgroundColor: const Color(0xFF6AA517));
                    return '';
                  }
                },
                controller: surnameController
            ),

            SizedBox(
              height: Get.height * 0.02,
            ),
            myTextField(
                bool: false,
                icon: 'assets/mail.png',
                text: 'Adresse email',
                validator: (String input){
                  if(input.isEmpty){
                    Get.snackbar(
                        'Attention',
                        'Adresse mail MGEN obligatoire',
                        colorText: Colors.white,backgroundColor:  const Color(0xFF6AA517));
                    return '';
                  }

                  if(!input.contains('@')){
                    Get.snackbar(
                        'Attention',
                        'Adresse mail inconnue',
                        colorText: Colors.white,backgroundColor:  const Color(0xFF6AA517));
                    return '';
                  }
                },
                controller: emailController
            ),

            SizedBox(
              height: Get.height * 0.02,
            ),
            myTextField(
                bool: true,
                icon: 'assets/lock.png',
                text: 'Mot de passe',
                validator: (String input){
                  if(input.isEmpty){
                    Get.snackbar(
                        'Attention',
                        'Mot de passe obligatoire',
                        colorText: Colors.white,
                        backgroundColor: const Color(0xFF6AA517));
                    return '';
                  }

                  if(input.length <3){
                    Get.snackbar('Attention',
                        'Mot de passe doit avoir au moins 3 characteres',
                         colorText: Colors.white,
                         backgroundColor: const Color(0xFF6AA517));
                    return '';
                  }
                },
                controller: passwordController
            ),

            SizedBox(
              height: Get.height * 0.02,
            ),
            myTextField(
                bool: true,
                icon: 'assets/lock.png',
                text: 'Entrez à nouveau votre mot de passe',
                validator: (input){
                  if(input != passwordController.text.trim()){
                    Get.snackbar(
                        'Attention',
                        'Le mot de passe renseigné est different',
                        colorText: Colors.white,backgroundColor: const Color(0xFF6AA517));
                    return '';
                  }
                },
              controller: confirmPasswordController
            ),

            Obx(() => authController.isLoading.value
                ? const Center(
              child: CircularProgressIndicator(),
            )
            : Container(
              height: 58,
              margin: EdgeInsets.symmetric(
                vertical: Get.height * 0.04,
              ),
              width: Get.width,
              child: ElevatedButton(
                onPressed: () {
                  if (!formKey.currentState!.validate()) {
                    return;
                  }

                  authController.signUp(
                      email: emailController.text.trim(),
                      password: passwordController.text.trim(),
                      name: nameController.text.trim(),
                      surname: surnameController.text.trim());
                },

                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(const Color(0xFF6AA517)),
                  shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
                child: const Text(
                  "S'INSCRIRE",
                  style: TextStyle(
                      fontSize: 18, color: Colors.white,
                      fontWeight: FontWeight.w500
                  ),
                ),
              ),
            ),
          ),

            SizedBox(
              height: Get.height * 0.01,
            ),
            SizedBox(
              height: Get.height * 0.02,
            ),
          ],
        )
    );
  }
}
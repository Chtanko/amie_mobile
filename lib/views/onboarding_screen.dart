import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'auth/login_signup.dart';

class OnBoardingScreen extends StatelessWidget {
  const OnBoardingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            // Ajout de l'image en arrière-plan
            Image.asset(
              'assets/background.png',
              fit: BoxFit.cover,
              width: double.infinity,
              height: double.infinity,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const SizedBox(
                  height: 50,
                ),
                const Text(
                  "A M I E",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 27,
                      fontWeight: FontWeight.w700),
                ),
                const SizedBox(
                  height: 5,
                ),
               const SizedBox(
                  height: 50,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  child: Image.asset('assets/logo_lab.png'),
                ),
              ],
            ),
        Positioned(
          bottom: MediaQuery.of(context).size.height * 0.1, // 20% par rapport au fond
          left: MediaQuery.of(context).size.width * 0.05, // 5% par rapport à la gauche
          right: MediaQuery.of(context).size.width * 0.05, // 5% par rapport à la droite
          child: MaterialButton(
                    height: 58,
                    minWidth: double.infinity,
                    color: const Color(0xFF6AA517), // Couleur du bouton en vert
                    elevation: 2,
                    onPressed: () {
                      Get.to(() => const LoginView());
                    },
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15), // Définition du borderRadius à 15
                    ),
                    child: const Text(
                      "ACCÉDER",
                      style: TextStyle(fontSize: 18, color: Colors.white, fontWeight: FontWeight.w500),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

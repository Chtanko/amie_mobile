import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart';
import 'dart:convert';
import '../../controller/auth_controller.dart';
import '../../widgets/custom_dialog.dart';
import 'package:http/http.dart' as http;

class DetailedEventInscription extends StatefulWidget {
  final Map event;

  const DetailedEventInscription({Key? key, required this.event}) : super(key: key);

  @override
  State<DetailedEventInscription> createState() => _DetailedEventInscriptionState();
}

class _DetailedEventInscriptionState extends State<DetailedEventInscription> {
  AuthController _authController = Get.find<AuthController>();

  Future<void> registerEvent() async {
    AuthController _authController = Get.find<AuthController>();
    int idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    int idEvenement = widget.event['idEvenement'];
    print('idUtilisateur: $idUtilisateur, idEvenement: $idEvenement');

    AuthController authController = Get.find<AuthController>();

    // Effectuer la requête d'inscription
    var response = await http.post(
      Uri.parse('http://amie.labinno-mtech.fr/api/evenement/$idUtilisateur/$idEvenement'),
      headers: <String, String>{
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    if (response.statusCode == 200) {
      // Inscription réussie
      // Afficher une boîte de dialogue ou une notification de succès
      var successDialog = CustomDialog(context);
      await successDialog.displayDialog('Succès', 'Inscription réussie');
    } else {
      // Imprimer le corps de la réponse pour obtenir plus d'informations sur l'erreur
      print('Erreur lors de l\'inscription : ${response.body}');
      var errorMessage = response.body;

      // Afficher une boîte de dialogue ou une notification d'erreur
      var errorDialog = CustomDialog(context);
      await errorDialog.displayDialog('Erreur', errorMessage);
    }
    print('idUtilisateur : $idUtilisateur, idEvenement : $idEvenement');
  }

  String formatDateRange(String startDateInput, String endDateInput) {
    if (startDateInput == null ||
        startDateInput.isEmpty ||
        endDateInput == null ||
        endDateInput.isEmpty) {
      return 'Pas de date';
    }

    var startDate = DateFormat('dd/MM/yyyy').parse(startDateInput);
    var endDate = DateFormat('dd/MM/yyyy').parse(endDateInput);
    var startDayFormatter = DateFormat('d', 'fr_FR');
    var startYearFormatter = DateFormat('y', 'fr_FR');
    var endDayFormatter = DateFormat('d', 'fr_FR');
    var monthFormatter = DateFormat('MMMM', 'fr_FR');
    var endDayMonthYearFormatter = DateFormat('d MMMM, y', 'fr_FR');

    // Check if the two dates are in the same month
    if (startDate.year == endDate.year && startDate.month == endDate.month) {
      return '${startDayFormatter.format(startDate)} - ${endDayFormatter.format(endDate)} ${monthFormatter.format(startDate)}, ${startYearFormatter.format(startDate)}';
    } else {
      return '${startDayFormatter.format(startDate)}, ${startYearFormatter.format(startDate)} - ${endDayMonthYearFormatter.format(endDate)}';
    }
  }

  Future<void> ajoutFavoris() async {
    // Récupérer l'id de l'utilisateur connecté et de l'événement
    AuthController _authController = Get.find<AuthController>();
    int idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    int idEvenement = widget.event['idEvenement'];
    print('idUtilisateur: $idUtilisateur, idEvenement: $idEvenement');

    var response = await http.post(
      Uri.parse('http://amie.labinno-mtech.fr/api/evenement/ajouterfavoris/$idUtilisateur/$idEvenement'),
      headers: <String, String>{
        'Authorization': _authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
    );

    if(response.statusCode == 200){
      var successDialog = CustomDialog(context);
      await successDialog.displayDialog('Succès', 'Ajout en favoris réussie');
    } else {
      print('Erreur lors de l\'ajout aux favoris : ${response.body}');

      // Récupérer le message d'erreur du backend
      var errorMessage = response.body;

      // Afficher une boîte de dialogue ou une notification d'erreur
      var errorDialog = CustomDialog(context);
      await errorDialog.displayDialog('Erreur', errorMessage);
    }
    print('idUtilisateur : $idUtilisateur, idEvenement : $idEvenement');
  }

  String formatDate(String input) {
    var date = DateTime.parse(input);
    var formatter = DateFormat('d MMM, y', 'fr_FR');
    return formatter.format(date);
  }

  String formatWeekDays(String startDateInput, String endDateInput) {
    if (startDateInput == null || startDateInput.isEmpty || endDateInput == null || endDateInput.isEmpty) {
      return 'Pas de date';
    }

    var startDate = DateFormat('dd/MM/yyyy').parse(startDateInput);
    var endDate = DateFormat('dd/MM/yyyy').parse(endDateInput);

    if (startDate == null || endDate == null) {
      return 'Pas de date';
    }

    var formatter = DateFormat('EEEE', 'fr_FR');
    String formattedStartDate = formatter.format(startDate);
    String formattedEndDate = formatter.format(endDate);

    return 'Du $formattedStartDate au $formattedEndDate';
  }

  List<Widget> _buildRolesWidgets(List roles) {
    List<Widget> rolesWidgets = [];
    for (var role in roles) {
      if (role['libelle'] != 'Administrateur') {
        rolesWidgets.add(Text(
          role['libelle'] ?? 'Aucun rôle',
          style: const TextStyle(fontSize: 14, color: Color(0xFF747688)),
        ));
        rolesWidgets.add(const Text(
          ' - ',
          style: TextStyle(fontSize: 14, color: Color(0xFF747688)),
        ));
      }
    }
    // Supprime le dernier ' - '
    if (rolesWidgets.isNotEmpty) {
      rolesWidgets.removeLast();
    }
    return rolesWidgets;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/background.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40.0),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                      Row(
                        children: [
                          IconButton(
                            icon: const Icon(Icons.arrow_back),
                            onPressed: () => Get.back(),
                          ),
                          const Text(
                            'Retour',
                            style: TextStyle(
                                fontSize: 20,
                                fontWeight: FontWeight.w300,
                                fontFamily: 'Inter'),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16.0),
                        Stack(
                          clipBehavior: Clip.none,
                          children: [
                            ClipRRect(
                              borderRadius: BorderRadius.circular(10),
                              child: Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                height: 142,
                                color: const Color(0xFFE0E0E0),
                                child: () {
                                  try {
                                    if (widget.event['image'] != null && widget.event['image'].trim().isNotEmpty) {
                                      return Image.memory(base64Decode(widget.event['image']),
                                        fit: BoxFit.cover,
                                      );
                                    }
                                  } catch (e) {
                                    if (kDebugMode) {
                                      print('Erreur de décodage de l\'image: $e');
                                    }
                                  }
                                  return Image.asset("assets/logo_background.png",
                                      fit: BoxFit.cover);
                                }(),
                              ),
                            ),
                            Positioned(
                              top: -20, // ajustez ces valeurs comme vous le souhaitez
                              right: -20, // ajustez ces valeurs comme vous le souhaitez
                              child: GestureDetector(
                                onTap: () {
                                  ajoutFavoris();
                                },
                                child: Material(
                                  elevation: 40.0, // ajustez cette valeur pour changer l'effet de surélévation
                                  color: Colors.transparent, // assurez-vous que le widget Material est transparent
                                  child: Image.asset(
                                    "assets/favoris.png",
                                    width: 50, // ajustez cette valeur comme vous le souhaitez
                                    height: 50, // ajustez cette valeur comme vous le souhaitez
                                  ),
                                ),
                              ),
                            ),

                            const Positioned(
                              top: -20, // ajustez ces valeurs comme vous le souhaitez
                              right: 30, // ajustez ces valeurs comme vous le souhaitez
                              child: Text(
                                'Ajouter en favoris',
                                style: TextStyle(
                                  fontSize: 12, // ajustez cette valeur comme vous le souhaitez
                                  color: Color(0xFF3B4549),
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16.0),
                      Align(
                        alignment: Alignment.centerLeft,
                        child: Padding(
                          padding: EdgeInsets.only(
                              left: MediaQuery.of(context).size.width * 0.1),
                          child: Text(
                            widget.event['label'] ?? 'Titre non trouvé',
                            style: const TextStyle(fontSize: 24),
                          ),
                        ),
                      ),
                      const SizedBox(height: 24.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    left: MediaQuery.of(context).size.width * 0.1),
                                child: Image.asset("assets/green_calendar.png"),
                              ),
                              const SizedBox(width: 18.0),
                              // Espace entre l'icône et le texte
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    formatDateRange(widget.event['date_debut'] ?? '', widget.event['date_fin'] ?? ''),
                                    style: const TextStyle(fontSize: 18),
                                    textAlign: TextAlign.left,
                                  ),
                                  Text(
                                    formatWeekDays(widget.event['date_debut'] ?? '', widget.event['date_fin'] ?? ''),
                                    style: const TextStyle(fontSize: 14, color: Color(0xFF747688)),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 24.0),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1),
                                child: Image.asset("assets/location_point.png"),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(left: 18.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${widget.event['lieu']['adresse'] ?? 'Pas d\'adresse'} ',
                                      style: const TextStyle(fontSize: 18),
                                    ),
                                    Text(
                                      '${widget.event['lieu']['ville'] ?? 'Ville absente'}'
                                      '${', '}'
                                      '${widget.event['lieu']['codePostal'] ?? 'Code postal absent'} ',
                                      style: const TextStyle(
                                          fontSize: 14,
                                          color: Color(0xFF747688)),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      const SizedBox(height: 24.0),
                      Row(
                        children: [
                          Padding(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.1),
                            child: Image.asset("assets/green_scholar.png"),
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 18.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    '${widget.event['utilisateur']['prenom'] ?? 'Aucun prénom'} ${widget.event['utilisateur']['nom'] ?? 'Aucun nom'}',
                                    style: const TextStyle(fontSize: 18),
                                  ),
                                  if (widget.event['utilisateur']['roles'] != null)
                                    Wrap(
                                      crossAxisAlignment:
                                      WrapCrossAlignment.center,
                                      children: _buildRolesWidgets(
                                          widget.event['utilisateur']['roles']),
                                    ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 16.0),
                      Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Text(
                          '${widget.event['description'] ?? 'Aucune description'}',
                          style: const TextStyle(fontSize: 16),
                        ),
                      ),
                      SizedBox(height: MediaQuery.of(context).size.height * 0.2),
                    ],
                  ),
                ),
              ),
              Center(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width * 0.8, // 80% of total width
                  height: 54,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      backgroundColor: const Color(0xFF6AA517),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15),
                      ),
                    ),
                    onPressed: () async {
                      var customDialog = CustomDialog(context);
                      bool? confirmationResult =
                      await customDialog.displayConfirmationDialog(
                        'Confirmation', 'Confirmer l\'inscription ?',
                      );
                      if (confirmationResult == true) {
                        await registerEvent();
                      }
                    },
                    child: const Text(
                      "S'INSCRIRE",
                      style: TextStyle(
                          fontSize: 18,
                          color: Colors.white,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

import 'dart:convert';
import 'package:ems/views/contributions/contributions_view.dart';
import 'package:ems/views/profile/profile_view.dart';
import 'package:ems/views/favourites/favourite_view.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../../controller/auth_controller.dart';
import '../../widgets/bottom_navbar_widget.dart';
import '../../widgets/custom_dialog.dart';
import '../home/home_screen.dart';
import 'package:flutter/rendering.dart' as rendering;
import 'package:image_picker/image_picker.dart';
import 'dart:io';
import 'package:characters/characters.dart';
import 'package:intl/intl.dart' as intl;

class CreateEvent extends StatefulWidget {
  const CreateEvent({Key? key}) : super(key: key);

  @override
  _CreateEventState createState() => _CreateEventState();
}

class _CreateEventState extends State<CreateEvent> {
  //final TextEditingController _idUtilisateurController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _dateDebutController = TextEditingController();
  final TextEditingController _dateFinController = TextEditingController();
  final TextEditingController _descriptionController = TextEditingController();
  final TextEditingController _heureDebutController = TextEditingController();
  final TextEditingController _heureFinController = TextEditingController();
  final TextEditingController _imageController = TextEditingController();
  final TextEditingController _labelController = TextEditingController();
  final TextEditingController _lienReplayController = TextEditingController();
  final TextEditingController _lienRessourcesController = TextEditingController();
  final TextEditingController _statutController = TextEditingController();
  final TextEditingController _proposedByController = TextEditingController();
  final ImagePicker _picker = ImagePicker();
  late AuthController _authController;
  late String idUtilisateur;
  late String idLieu;
  late String idTypologieEvenements;
  CustomDialog? customDialog;

  List<String> _typologieLabels = [];
  List<String> _lieuxLabels = [];

  List<String> _selectedLabels = []; // Pour stocker les labels sélectionnés
  List<String> _selectedTypologieIds = []; // Pour stocker les ids de typologie sélectionnés

  int _selectedIndex = 2;
  bool _isItemSelected = true;
  String? _selectedLabel;
  bool _imagePicked = false;
  XFile? _pickedImage;
  String? _selectedLieuId; // Initialisation de l'ID du lieu sélectionné
  String? _selectedTypologieId;
  List<String> _typologieIds = []; // Initialisation de la liste des ID des typologies
  List<String> _lieuxIds = []; // Initialisation de la liste des ID des lieux

  Future<void> _pickImage() async {
    final pickedImage = await _picker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      _pickedImage = pickedImage;

      final bytes = await File(_pickedImage!.path).readAsBytes();

      print('Image size in bytes: ${bytes.length}'); // Logging the image size

      _imageController.text = base64Encode(bytes);
      print('Encodage de l\'image réussi'); // Logging successful encoding

      if (_imageController.text.length >= 10) {
        print('Image Base64 String (premiers 20 characteres): ${_imageController.text.substring(0, 20)}');
      } else {
        print('Image Base64 String: ${_imageController.text}');
      }

      setState(() {
        _imagePicked = true;
      });
    } else {
      print('Aucune image sélectionnée'); // Logging when no image is selected
    }
  }

// Method to determine the status of the event
  String getStatut(DateTime debut, DateTime fin) {
    final maintenant = DateTime.now();
    if (debut.isAfter(maintenant) && fin.isAfter(maintenant)) {
      return 'En attente';
    } else if (maintenant.isAfter(debut) && maintenant.isBefore(fin)) {
      return 'En cours';
    } else {
      return 'Terminé';
    }
  }

  TimeOfDay selectedTimeDebut = TimeOfDay.now();
  TimeOfDay selectedTimeFin = TimeOfDay.now();

  Future<void> _selectTimeDebut(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTimeDebut,
    );
    if (picked != null && picked != selectedTimeDebut) {
      selectedTimeDebut = picked;
      _heureDebutController.text = picked.format(context); // format is HH:mm

      if (_dateDebutController.text.isNotEmpty &&
          _heureFinController.text.isNotEmpty) {
        final dateDebut =
            DateFormat('dd/MM/yyyy', 'fr_FR').parse(_dateDebutController.text);
        final heureDebutParts = _heureDebutController.text.split(' : ');
        final dateFin =
            DateFormat('dd/MM/yyyy', 'fr_FR').parse(_dateFinController.text);
        final heureFinParts = _heureFinController.text.split(' : ');

        final debut = DateTime(
          dateDebut.year,
          dateDebut.month,
          dateDebut.day,
          int.parse(heureDebutParts[0]),
          int.parse(heureDebutParts[1]),
        );
        final fin = DateTime(
          dateFin.year,
          dateFin.month,
          dateFin.day,
          int.parse(heureFinParts[0]),
          int.parse(heureFinParts[1]),
        );
        _statutController.text = getStatut(debut, fin);
      }
    }
  }

  Future<void> _selectTimeFin(BuildContext context) async {
    final TimeOfDay? picked = await showTimePicker(
      context: context,
      initialTime: selectedTimeFin,
    );
    if (picked != null && picked != selectedTimeFin) {
      selectedTimeFin = picked;
      _heureFinController.text = picked.format(context); // format is HH:mm

      if (_dateDebutController.text.isNotEmpty &&
          _heureDebutController.text.isNotEmpty) {
        final dateDebut =
            DateFormat('dd/MM/yyyy', 'fr_FR').parse(_dateDebutController.text);
        final heureDebutParts = _heureDebutController.text.split(':');
        final dateFin =
            DateFormat('dd/MM/yyyy', 'fr_FR').parse(_dateFinController.text);
        final heureFinParts = _heureFinController.text.split(':');

        final debut = DateTime(
          dateDebut.year,
          dateDebut.month,
          dateDebut.day,
          int.parse(heureDebutParts[0]),
          int.parse(heureDebutParts[1]),
        );
        final fin = DateTime(
          dateFin.year,
          dateFin.month,
          dateFin.day,
          int.parse(heureFinParts[0]),
          int.parse(heureFinParts[1]),
        );

        _statutController.text = getStatut(debut, fin);
      }
    }
  }

  Future<double> getMaxLabelWidth(List<String> labels, TextStyle style) async {
    final TextPainter textPainter = TextPainter(
      textDirection: rendering.TextDirection.ltr,
    );

    double maxWidth = 0;
    for (String label in labels) {
      textPainter.text = TextSpan(text: label, style: style);
      textPainter.layout();
      if (textPainter.width > maxWidth) {
        maxWidth = textPainter.width;
      }
    }
    return maxWidth;
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      Get.to(
        () => const HomeScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 1) {
      Get.to(
        () => const FavouriteScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 2) {
      Get.to(
        () => const ContributionsScreen(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    } else if (index == 3) {
      Get.to(
        () => const ProfileView(),
        transition: Transition.fadeIn,
        duration: const Duration(milliseconds: 500),
      );
    }
    setState(() {
      if (_isItemSelected && _selectedIndex == index) {
        _isItemSelected = false;
      } else {
        _isItemSelected = true;
        _selectedIndex = index;
      }
    });
  }

  @override
  void initState() {
    super.initState();
    _authController = Get.find<AuthController>();
    _getTypologieEvenements();
    _proposedByController.text =
        '${_authController.loggedInUser.value.prenom} ${_authController.loggedInUser.value.nom}';
    fetchLieux();
    //idUtilisateur = _idUtilisateurController.text;
    idUtilisateur = _authController.loggedInUser.value.idUtilisateur.toString();
    customDialog = CustomDialog(context);
  }

  Future<void> _addEvent() async {

    print("Ajout de l'événement...");

    if (_authController.loggedInUser.value != null &&
        _authController.loggedInUser.value!.idUtilisateur != null) {
      idUtilisateur = _authController.loggedInUser.value!.idUtilisateur.toString();
      print("idUtilisateur: $idUtilisateur");
    } else {
      print("Aucun utilisateur connecté ou idUtilisateur est null");
      return; // Retourner si aucun utilisateur n'est connecté ou si idUtilisateur est null
    }

    int idLieu = _selectedLieuId != null ? int.parse(_selectedLieuId!) : 0;

    // Utilisez la liste _selectedTypologieIds au lieu de _typologieIds
    List<int> idTypologieEvenements = _selectedTypologieIds.map((id) => int.parse(id)).toList();

    final String baseUrl = "http://amie.labinno-mtech.fr/api/evenement/createevenement/$idUtilisateur/$idLieu/";

    String fullUrl = baseUrl;

    var evenementRequest = {
      "date_debut": _dateDebutController.text,
      "date_fin": _dateFinController.text,
      "description": _descriptionController.text,
      "heure_debut": _heureDebutController.text,
      "heure_fin": _heureFinController.text,
      "idTypologieEvenements": idTypologieEvenements, // Ajouter cette ligne
      "image": _imageController.text,
      "label": _labelController.text,
      "lien_replay": _lienReplayController.text,
      "lien_ressources": _lienRessourcesController.text,
      "statut": _statutController.text
    };

    AuthController authController = Get.find<AuthController>();

    var response = await http.post(
      Uri.parse(fullUrl),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
      },
      body: jsonEncode(evenementRequest),
    );

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200 || response.statusCode == 201) {
      print("Événement ajouté avec succès");
    } else {
      print("Échec de l'ajout de l'événement");
    }
  }

  Future<void> fetchLieux() async {
    const String url = 'http://amie.labinno-mtech.fr/api/lieu/getalllieux';

    //Récupération du AuthController
    AuthController authController = Get.find<AuthController>();

    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
      },
    );

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      final List<dynamic> data = jsonDecode(response.body);
      setState(() {
        _lieuxLabels = data
            .map((e) => e['adresse'] + ', ' + e['ville'] as String)
            .toList();
        _lieuxIds = data.map((e) => e['idLieu'].toString()).toList();
      });
    } else {
      throw Exception('Erreur de récupération des lieux');
    }
  }

  Map<String, String> _typologieMap = {};

  Future<void> _getTypologieEvenements() async {
    const String url = "http://amie.labinno-mtech.fr/api/typologieevenement/getalltypologieevenements";

    // Récupération du AuthController
    AuthController authController = Get.find<AuthController>();

    // Utilisation du token stocké dans le AuthController
    final response = await http.get(
      Uri.parse(url),
      headers: {
        'Authorization': authController.accessToken.value,
        'accept': '*/*',
      },
    );

    print('Response status: ${response.statusCode}');
    print('Response body: ${response.body}');

    if (response.statusCode == 200) {
      final List<dynamic> data = jsonDecode(response.body);
      setState(() {
        _typologieLabels = data.map((e) => e['label'] as String).toList();
        _typologieIds = data.map((e) => e['idTypologieEvenements'].toString()).toList();
      });
      print("_typologieMap : $_typologieMap");
    } else {
      ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(
            content: Text('Erreur lors de la récupération des typologies')),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 40.0),
          child: Column(
            children: [
              Row(
                children: [
                  IconButton(
                    icon: const Icon(Icons.arrow_back),
                    onPressed: () => Get.back(),
                  ),
                  const Text(
                    'Ajouter un événement',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w300,
                        fontFamily: 'Inter'
                    ),
                  ),
                ],
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Form(
                    key: _formKey,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        children: <Widget>[
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                // 5% de l'écran de chaque côté
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color(0xFFC7C6C6),
                                    // Couleur du bord
                                    width: 1.0, // Épaisseur du bord
                                  ),
                                  borderRadius: BorderRadius.circular(
                                      15), // Rayon d'arrondissement des bords
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8.0),
                                  // ajustez ce padding comme vous voulez
                                  child: Column(
                                    children: <Widget>[
                                      TextFormField(
                                        controller: _labelController,
                                        decoration: const InputDecoration(
                                          labelText: "Nom de l'évènement*",
                                          border:
                                              UnderlineInputBorder(), // pour ajouter une bordure en bas du champ
                                        ),
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return "Veuillez nommez l'évènement";
                                          }
                                          return null;
                                        },
                                      ),
                                      TextFormField(
                                        controller: _proposedByController,
                                        decoration: const InputDecoration(
                                          labelText: "Proposé par : ",
                                          border:
                                              UnderlineInputBorder(), // pour ajouter une bordure en bas du champ
                                        ),
                                        readOnly:
                                            true, // Pour rendre le champ non modifiable
                                      ),
                                      const SizedBox(height: 18.0),
                                      // Ajoute un espace vertical entre les deux champs
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          Column(
                            children: [
                              FutureBuilder<double>(
                                future: getMaxLabelWidth(_typologieLabels,
                                    Theme.of(context).textTheme.bodyText2!),
                                builder: (context, snapshot) {
                                  // Check if snapshot has an error
                                  if (snapshot.hasError) {
                                    return Text('Erreur: ${snapshot.error}');
                                  }
                                  // Check if snapshot has data
                                  if (snapshot.hasData) {
                                    final maxLabelWidth = snapshot.data!;
                                    return SizedBox(
                                      height: 33,
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: _typologieLabels.length,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                            onTap: () {
                                              print(
                                                  'Index sélectionné: $index');
                                              print(
                                                  'Valeur à l\'index: ${_typologieIds[index]}');
                                              setState(() {
                                                if (_selectedLabels.contains(
                                                    _typologieLabels[index])) {
                                                  // Si le label est déjà sélectionné, on le retire de la liste
                                                  _selectedLabels.remove(
                                                      _typologieLabels[index]);
                                                  _selectedTypologieIds.remove(
                                                      _typologieIds[index]);
                                                } else {
                                                  // Sinon, on l'ajoute à la liste
                                                  _selectedLabels.add(
                                                      _typologieLabels[index]);
                                                  _selectedTypologieIds.add(
                                                      _typologieIds[index]);
                                                }
                                              });
                                              print(
                                                  'Labels sélectionnés: $_selectedLabels');
                                              print(
                                                  'Ids de typologie sélectionnés: $_selectedTypologieIds');
                                            },
                                            child: Container(
                                              width: maxLabelWidth + 16,
                                              padding:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 8,
                                                      vertical: 4),
                                              margin:
                                                  const EdgeInsets.symmetric(
                                                      horizontal: 4),
                                              decoration: BoxDecoration(
                                                color: _selectedLabels.contains(
                                                        _typologieLabels[index])
                                                    ? const Color(0xFF6AA517)
                                                    : Colors.grey[300],
                                                borderRadius:
                                                    BorderRadius.circular(40),
                                              ),
                                              child: Center(
                                                child: Text(
                                                  (() {
                                                    try {
                                                      return utf8.decode(
                                                          _typologieLabels[
                                                                  index]
                                                              .runes
                                                              .toList());
                                                    } catch (e) {
                                                      print(
                                                          'Erreur de décodage UTF-8: $e');
                                                      return _typologieLabels[
                                                          index]; // ou une valeur par défaut de votre choix
                                                    }
                                                  }()),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    );
                                  }

                                  // Default widget to return if data is still loading or an error occurred
                                  return CircularProgressIndicator();
                                },
                              ),
                            ],
                          ),
                          const SizedBox(height: 18.0),
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                child: DropdownButtonFormField<String>(
                                  onChanged: (newValue) {
                                    setState(() {
                                      _selectedLieuId = _lieuxIds[
                                          _lieuxLabels.indexOf(newValue!)];
                                      if (_selectedLieuId != null) {
                                        _lieuxIds.add(_selectedLieuId!);
                                      }
                                    });
                                    print('ID sélectionné: $_selectedLieuId');
                                  },
                                  items: _lieuxLabels
                                      .map<DropdownMenuItem<String>>((value) {
                                    return DropdownMenuItem<String>(
                                      value: value,
                                      child: Text(
                                          utf8.decode(value.runes.toList())),
                                    );
                                  }).toList(),
                                  selectedItemBuilder: (BuildContext context) {
                                    return _lieuxLabels
                                        .map<Widget>((String value) {
                                      return SizedBox(
                                        width: constraints.maxWidth * 0.9,
                                        child: Text(
                                          utf8.decode(value.runes.toList()),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      );
                                    }).toList();
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Localisation',
                                    contentPadding: const EdgeInsets.all(10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: const BorderSide(
                                        color: Colors.grey,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                  isExpanded: true,
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color(0xFFC7C6C6),
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8.0),
                                  child: Column(
                                    children: <Widget>[
                                      TextFormField(
                                        controller: _dateDebutController,
                                        textAlign: TextAlign.right,
                                        decoration: const InputDecoration(
                                          labelText: 'Date de début',
                                        ),
                                        onTap: () async {
                                          final date = await showDatePicker(
                                            context: context,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(2000),
                                            lastDate: DateTime(2100),
                                            builder: (BuildContext context,
                                                Widget? child) {
                                              return Theme(
                                                data:
                                                    Theme.of(context).copyWith(
                                                  colorScheme:
                                                      const ColorScheme.light(
                                                    primary: Color(0xFF6AA517),
                                                    onPrimary: Colors.white,
                                                  ),
                                                ),
                                                child: child!,
                                              );
                                            },
                                          );
                                          if (date != null) {
                                            final dateTime = DateTime(date.year,
                                                date.month, date.day);

                                            // Use the isoDateTime for saving to your database
                                            final isoDateTime =
                                                dateTime.toIso8601String();

                                            // Display the date in French format for the user
                                            final text = DateFormat(
                                                    'dd/MM/yyyy', 'fr_FR')
                                                .format(date);

                                            // Use the French formatted date for display
                                            _dateDebutController.text = text;
                                          }
                                        },
                                      ),
                                      InkWell(
                                        onTap: () => _selectTimeDebut(context),
                                        // Here is the change, use _selectTimeDebut for _heureDebutController
                                        child: IgnorePointer(
                                          child: TextFormField(
                                            controller: _heureDebutController,
                                            textAlign: TextAlign.right,
                                            // Text will be aligned to the right
                                            decoration: InputDecoration(
                                              labelText: 'Heure de début',
                                              hintText: _heureDebutController
                                                      .text.isEmpty
                                                  ? 'Appuyer pour choisir'
                                                  : null, // Display hint text when there is no text in the TextField
                                            ),
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Veuillez entrer une heure de début';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 18.0),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Container(
                                margin: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: const Color(0xFFC7C6C6),
                                    width: 1.0,
                                  ),
                                  borderRadius: BorderRadius.circular(15),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8.0, vertical: 8.0),
                                  child: Column(
                                    children: <Widget>[
                                      TextFormField(
                                        controller: _dateFinController,
                                        textAlign: TextAlign.right,
                                        decoration: const InputDecoration(
                                          labelText: 'Date de fin',
                                        ),
                                        onTap: () async {
                                          final date = await showDatePicker(
                                            context: context,
                                            initialDate: DateTime.now(),
                                            firstDate: DateTime(2000),
                                            lastDate: DateTime(2100),
                                            builder: (BuildContext context,
                                                Widget? child) {
                                              return Theme(
                                                data:
                                                    Theme.of(context).copyWith(
                                                  colorScheme:
                                                      const ColorScheme.light(
                                                    primary: Color(0xFF6AA517),
                                                    //highlight color and header background color
                                                    onPrimary: Colors
                                                        .white, //text Color
                                                  ),
                                                ),
                                                child: child!,
                                              );
                                            },
                                          );
                                          if (date != null) {
                                            final dateTime = DateTime(date.year,
                                                date.month, date.day);

                                            // Use the isoDateTime for saving to your database
                                            final isoDateTime =
                                                dateTime.toIso8601String();

                                            // Display the date in French format for the user
                                            final text = DateFormat(
                                                    'dd/MM/yyyy', 'fr_FR')
                                                .format(date);

                                            _dateFinController.text = text;
                                          }
                                        },
                                        validator: (value) {
                                          if (value == null || value.isEmpty) {
                                            return 'Veuillez entrer une date de fin';
                                          }

                                          // Parse date from the date controllers
                                          DateTime dateDebut = DateFormat(
                                                  'dd/MM/yyyy', 'fr_FR')
                                              .parse(_dateDebutController.text);
                                          DateTime dateFin = DateFormat(
                                                  'dd/MM/yyyy', 'fr_FR')
                                              .parse(_dateFinController.text);

                                          if (dateFin.isBefore(dateDebut)) {
                                            return 'La date de fin ne peut pas être avant la date de début';
                                          }
                                          return null;
                                        },
                                      ),
                                      InkWell(
                                        onTap: () => _selectTimeFin(context),
                                        child: IgnorePointer(
                                          child: TextFormField(
                                            controller: _heureFinController,
                                            textAlign: TextAlign.right,
                                            // Text will be aligned to the right
                                            decoration: InputDecoration(
                                              labelText: 'Heure de fin',
                                              hintText: _heureFinController
                                                      .text.isEmpty
                                                  ? 'Appuyer pour choisir'
                                                  : null, // Display hint text when there is no text in the TextField
                                            ),
                                            validator: (value) {
                                              if (value == null ||
                                                  value.isEmpty) {
                                                return 'Veuillez entrer une heure de fin';
                                              }
                                              return null;
                                            },
                                          ),
                                        ),
                                      ),
                                      const SizedBox(height: 18.0),
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                child: TextFormField(
                                  controller: _statutController,
                                  readOnly: true, // Make the field read-only
                                  decoration: InputDecoration(
                                    labelText: 'Statut',
                                    contentPadding: const EdgeInsets.all(10),
                                    enabled: false, // Gray out the field
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: const BorderSide(
                                        color: Colors.grey,
                                        width: 1.0,
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          LayoutBuilder(
                            builder: (context, constraints) {
                              return Padding(
                                padding: EdgeInsets.symmetric(
                                    horizontal: constraints.maxWidth * 0.05),
                                child: TextFormField(
                                  controller: _descriptionController,
                                  maxLines: 10,
                                  // Définir le nombre de lignes souhaité pour le champ de texte
                                  decoration: InputDecoration(
                                    labelText: 'Description*',
                                    contentPadding: const EdgeInsets.all(10),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15),
                                      borderSide: const BorderSide(
                                        color: Colors.grey,
                                        width: 1.0,
                                      ),
                                    ),
                                    labelStyle: const TextStyle(),
                                    floatingLabelBehavior:
                                        FloatingLabelBehavior.always,
                                  ),
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Veuillez entrer une description';
                                    }
                                    return null;
                                  },
                                ),
                              );
                            },
                          ),
                          const SizedBox(height: 18.0),
                          Padding(
                            padding: const EdgeInsets.only(top: 18.0),
                            child: GestureDetector(
                              onTap: () {
                                _pickImage();
                                print(
                                    'Image string in base64: ${_imageController.text}');
                              },
                              child: Container(
                                width: 308, // Adjust as needed
                                height: 112, // Adjust as needed
                                decoration: BoxDecoration(
                                  color: Colors.grey[200],
                                  borderRadius: BorderRadius.circular(15),
                                  border: Border.all(
                                      color: Colors.grey[400]!, width: 1),
                                ),
                                child: _imagePicked
                                    ? ClipRRect(
                                        borderRadius: BorderRadius.circular(15),
                                        child: Image.memory(
                                          base64Decode(_imageController.text),
                                          width: 308, // Adjust as needed
                                          height: 112, // Adjust as needed
                                          fit: BoxFit.cover,
                                        ),
                                      )
                                    : Center(
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: const <Widget>[
                                            Icon(
                                              Icons.add,
                                              size: 40,
                                              color: Colors.grey,
                                            ),
                                            Text(
                                              'Télécharger une image de couverture',
                                              textAlign: TextAlign.center,
                                              style:
                                                  TextStyle(color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                      ),
                              ),
                            ),
                          ),
                          const SizedBox(height: 18.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: TextFormField(
                              controller: _lienReplayController,
                              decoration: InputDecoration(
                                labelText: 'Lien de l\'évènement en ligne',
                                contentPadding: EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  _lienReplayController.text = "En attente";
                                }
                                return null;
                              },
                            ),
                          ),
                          const SizedBox(height: 18.0),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 10.0),
                            child: TextFormField(
                              controller: _lienRessourcesController,
                              decoration: InputDecoration(
                                labelText: 'Lien externe vers un contenu',
                                contentPadding: const EdgeInsets.all(10),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15),
                                ),
                              ),
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  _lienRessourcesController.text = "En attente";
                                }
                                return null;
                              },
                            ),
                          ),
                          const SizedBox(height: 40.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            // Centrer les boutons horizontalement
                            children: [
                              SizedBox(
                                width: 150,
                                height: 54,
                                child: OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                    foregroundColor: const Color(0xFF294218),
                                    backgroundColor: Colors.white,
                                    side: const BorderSide(
                                        color: Color(0xFFE5E5E5), width: 1),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  onPressed: () async {
                                    var customDialog = CustomDialog(context);
                                    bool? confirmationResult = await customDialog?.displayConfirmationDialog(
                                      'Annulation',
                                      'Êtes-vous sûr de vouloir abandonner la création de l\'événement ?',
                                    );

                                    if (confirmationResult == true) {
                                      Navigator.of(context)
                                          .pop(); // Navigate to home page
                                    }
                                  },
                                  child: const Text(
                                    "ANNULER",
                                    style: TextStyle(
                                        fontSize: 18,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 150,
                                height: 54,
                                child: ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    backgroundColor: const Color(0xFF6AA517),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                  ),
                                  onPressed: () async {
                                    print(
                                        'Proposé par: ${_proposedByController.text}');
                                    var customDialog = CustomDialog(context);
                                    bool? confirmationResult = await customDialog.displayConfirmationDialog(
                                      'Confirmation',
                                      'Êtes-vous sûr de vouloir publier l\'événement ?',
                                    );

                                    if (confirmationResult == true) {
                                      try {
                                        await _addEvent();
                                        // Ajout une boîte de dialogue ici pour confirmer que l'événement a été publié avec succès.
                                        await customDialog.displayDialog(
                                          'Succès',
                                          'L\'événement a été publié avec succès.',
                                        );
                                      } catch (e) {
                                        print(
                                            'Erreur lors de l\'ajout de l\'événement: $e');
                                        // Affichez un message à l'utilisateur ici pour lui dire que quelque chose s'est mal passé.
                                      }
                                    }
                                  },
                                  child: const Text(
                                    "PUBLIER",
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.white,
                                        fontWeight: FontWeight.w500),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavBar(
        selectedIndex: _selectedIndex,
        onItemTapped: _onItemTapped,
        unselected: true,
      ),
    );
  }
}

import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import '../../controller/auth_controller.dart';
import '../../widgets/custom_dialog.dart';
import '../home/home_screen.dart';

class DetailedEventRegister extends StatefulWidget {
  final Map event;

  const DetailedEventRegister({Key? key, required this.event}) : super(key: key);

  @override
  State<DetailedEventRegister> createState() => _DetailedEventRegisterState();
}

class _DetailedEventRegisterState extends State<DetailedEventRegister> {

  Future<void> registerEvent() async {
    // Récupérer l'id de l'utilisateur et de l'événement
    int idUtilisateur = widget.event['utilisateur']['idUtilisateur'];
    int idEvenement = widget.event['idEvenement'];
    print('idUtilisateur: $idUtilisateur, idEvenement: $idEvenement');
    // Afficher les détails de l'événement
    print('Événement: ${widget.event}');
  }

  Future<void> deleteInscription(int idEvenement) async {
    AuthController _authController = Get.find<AuthController>();
    var idUtilisateur = _authController.loggedInUser.value.idUtilisateur;
    print(idEvenement);

    var response = await http.delete(

        Uri.parse('http://amie.labinno-mtech.fr/api/evenement/deleteinscription/$idEvenement/$idUtilisateur'),

        headers: {
        'Authorization': _authController.accessToken.value,
        'accept': '*/*',
        'Content-Type': 'application/json'
        },
    );

        print('Response body: ${response.body}');
        print('Response status: ${response.statusCode}');

    if (response.statusCode != 200) {
      throw Exception('Erreur lors de la suppression de l\'inscription');
    }

  }

  @override
  void initState() {
    super.initState();
    registerEvent();  // Appeler registerEvent ici
  }

  String formatDateRange(String startDateInput, String endDateInput) {
    if (startDateInput == null ||startDateInput.isEmpty || endDateInput == null || endDateInput.isEmpty) {
      return 'Pas de date';
    }

    var startDate = DateTime.tryParse(startDateInput);
    var endDate = DateTime.tryParse(endDateInput);

    if (startDate == null || endDate == null) {
      return 'Pas de date';
    }

    var startDayFormatter = DateFormat('d', 'fr_FR');
    var startYearFormatter = DateFormat('y', 'fr_FR');
    var endDayFormatter = DateFormat('d', 'fr_FR');
    var monthFormatter = DateFormat('MMMM', 'fr_FR');
    var endDayMonthYearFormatter = DateFormat('d MMMM, y', 'fr_FR');

    // Check if the two dates are in the same month
    if (startDate.year == endDate.year && startDate.month == endDate.month) {
      return '${startDayFormatter.format(startDate)} - ${endDayFormatter.format(endDate)} ${monthFormatter.format(startDate)}, ${startYearFormatter.format(startDate)}';
    } else {
      return '${startDayFormatter.format(startDate)}, ${startYearFormatter.format(startDate)} - ${endDayMonthYearFormatter.format(endDate)}';
    }
  }

  String formatDate(String input) {
    var date = DateTime.parse(input);
    var formatter = DateFormat('d MMM, y', 'fr_FR');
    return formatter.format(date);
  }

  String formatWeekDays(String startDateInput, String endDateInput) {
    try {
      if (startDateInput == null || startDateInput.isEmpty || endDateInput == null || endDateInput.isEmpty) {
        return 'Pas de date';
      }

      var startDate = DateFormat('dd/MM/yyyy').parse(startDateInput);
      var endDate = DateFormat('dd/MM/yyyy').parse(endDateInput);

      if (startDate == null || endDate == null) {
        return 'Pas de date';
      }

      var formatter = DateFormat('EEEE', 'fr_FR');
      String formattedStartDate = formatter.format(startDate);
      String formattedEndDate = formatter.format(endDate);

      return 'Du $formattedStartDate au $formattedEndDate';
    } catch (e) {
      return 'Erreur de format de date';
    }
  }

  List<Widget> _buildRolesWidgets(List roles) {
    List<Widget> rolesWidgets = [];
    for (var role in roles) {
      if (role['libelle'] != 'Administrateur') {
        rolesWidgets.add(Text(
          role['libelle'] ?? 'Aucun rôle',
          style: const TextStyle(fontSize: 14, color: Color(0xFF747688)),
        ));
        rolesWidgets.add(const Text(
          ' - ',
          style: TextStyle(fontSize: 14, color: Color(0xFF747688)),
        ));
      }
    }
    // Supprime le dernier ' - '
    if (rolesWidgets.isNotEmpty) {
      rolesWidgets.removeLast();
    }
    return rolesWidgets;
  }

  @override
  Widget build(BuildContext context) {
    String label;
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/background.png"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40.0),
            child: Column(
              children: [
                Expanded(
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        Row(
                          children: [
                            IconButton(
                              icon: const Icon(Icons.arrow_back),
                              onPressed: () => Get.back(),
                            ),
                            const Text(
                              'Retour',
                              style: TextStyle(
                                  fontSize: 20,
                                  fontWeight: FontWeight.w300,
                                  fontFamily: 'Inter'),
                            ),
                          ],
                        ),
                        const SizedBox(height: 16.0),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Container(
                            width: MediaQuery.of(context).size.width * 0.8,
                            height: 142,
                            color: const Color(0xFFE0E0E0),
                            child: () {
                              try {
                                if (widget.event['image'] != null && widget.event['image'].trim().isNotEmpty) {
                                  return Image.memory(base64Decode(widget.event['image']),
                                    fit: BoxFit.cover,
                                  );
                                }
                              } catch (e) {
                                if (kDebugMode) {
                                  print('Erreur de décodage de l\'image: $e');
                                }
                              }
                              return Image.asset("assets/logo_background.png",
                                  fit: BoxFit.cover
                              );
                            }(),
                          ),
                        ),

                        const SizedBox(height: 16.0),
                        Align(
                          alignment: Alignment.centerLeft,
                          child: Padding(
                            padding: EdgeInsets.only(
                                left: MediaQuery.of(context).size.width * 0.1),
                            child: Text(
                              label = utf8.decode((widget.event['label'] ?? 'Titre non trouvé').runes.toList(), allowMalformed: true),
                              style: const TextStyle(fontSize: 24),
                            ),
                          ),
                        ),
                        const SizedBox(height: 24.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(
                                      left: MediaQuery.of(context).size.width * 0.1),
                                  child: Image.asset("assets/green_calendar.png"),
                                ),
                                const SizedBox(width: 18.0),
                                // Espace entre l'icône et le texte
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      formatDateRange(widget.event['date_debut'] ?? '', widget.event['date_fin'] ?? ''),
                                      style: const TextStyle(fontSize: 18),
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      formatWeekDays(widget.event['date_debut'] ?? '', widget.event['date_fin'] ?? ''),
                                      style: const TextStyle(fontSize: 14, color: Color(0xFF747688)),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Row(
                              children: [
                                Padding(
                                  padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.1),
                                  child: Image.asset("assets/location_point.png"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(left: 18.0),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text('${widget.event['lieu']['adresse'] ?? 'Pas d\'adresse'} ',
                                        style: const TextStyle(fontSize: 18),
                                      ),
                                      Text('${widget.event['lieu']['ville'] ?? 'Ville absente'}' '${', '}' '${widget.event['lieu']['codePostal'] ?? 'Code postal absent'} ',
                                        style: const TextStyle(
                                            fontSize: 14,
                                            color: Color(0xFF747688)),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                        const SizedBox(height: 24.0),
                        Row(
                          children: [
                            Padding(
                              padding: EdgeInsets.only(
                                  left: MediaQuery.of(context).size.width * 0.1),
                              child: Image.asset("assets/green_scholar.png"),
                            ),
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 18.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      '${widget.event['utilisateur']['prenom'] ?? 'Aucun prénom'} ${widget.event['utilisateur']['nom'] ?? 'Aucun nom'}',
                                      style: const TextStyle(fontSize: 18),
                                    ),
                                    if (widget.event['utilisateur']['roles'] != null)
                                      Wrap(
                                        crossAxisAlignment:
                                        WrapCrossAlignment.center,
                                        children: _buildRolesWidgets(
                                            widget.event['utilisateur']['roles']),
                                      ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                        const SizedBox(height: 26.0),

                        Align(
                          alignment: Alignment.center,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width * 0.8, // 80% of total width
                            child: Text(
                              utf8.decode((widget.event['description'] ?? 'Aucune description').runes.toList(), allowMalformed: true),
                              style: const TextStyle(fontSize: 16, height: 1.5), // Ajout de la hauteur pour l'espacement des lignes
                              textAlign: TextAlign.center,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Center(
                  child: SizedBox(
                    width: MediaQuery.of(context).size.width * 0.8, // 80% of total width
                    height: 54,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.05),
              ],
            ),
          ),

          Positioned(
            bottom: MediaQuery.of(context).size.height * 0.1, // 10% du bas de la page
            right: MediaQuery.of(context).size.width * 0.1, // 10% du bord droit de la page
            child: GestureDetector(
              onTap: () async {
                if (widget.event['idEvenement'] == null) {
                  print('Erreur : id de l\'événement est null');
                  return;
                }
                var customDialog = CustomDialog(context);
                bool? confirmationResult = await customDialog.displayConfirmationDialog(
                  'Confirmation', 'Voulez-vous vraiment vous désinscrire de ce superbe évènement ?',
                );
                if (confirmationResult == true) {
                  await deleteInscription(widget.event['idEvenement']);
                  Get.offAll(
                        () => const HomeScreen(),
                    transition: Transition.rightToLeftWithFade, // Transition de droite à gauche avec un fondu
                    duration: const Duration(milliseconds: 500), // Durée de l'animation
                  );
                }
              },

              child: const Material(
                elevation: 40.0, // ajustez cette valeur pour changer l'effet de surélévation
                color: Colors.transparent, // assurez-vous que le widget Material est transparent
                child: Image(
                  image: AssetImage('assets/icone_supprimer.png'),
                  width: 50, // ajustez cette valeur comme vous le souhaitez
                  height: 50, // ajustez cette valeur comme vous le souhaitez
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
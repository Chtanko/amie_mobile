import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class CustomCard extends StatelessWidget {
  final Map<String, dynamic> event;
  final BuildContext context;

  const CustomCard({Key? key, required this.event, required this.context})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    DateTime? dateDebut = DateTime.tryParse(event['date_debut'] ?? "");
    DateTime? dateFin = DateTime.tryParse(event['date_fin'] ?? "");
    TimeOfDay? heureDebut = (event['heure_debut'] != null)
        ? TimeOfDay.fromDateTime(
        DateTime.parse("1970-01-01 " + event['heure_debut']))
        : null;
    TimeOfDay? heureFin = (event['heure_fin'] != null)
        ? TimeOfDay.fromDateTime(
        DateTime.parse("1970-01-01 " + event['heure_fin']))
        : null;

    String formattedDateDebut =
    (dateDebut != null) ? DateFormat('d MMM yyyy', 'fr_FR').format(dateDebut) : 'Absent';
    String formattedDateFin =
    (dateFin != null) ? DateFormat('d MMM yyyy', 'fr_FR').format(dateFin) : 'Absent';
    String formattedHeureDebut =
    (heureDebut != null) ? heureDebut.format(context) : 'Absent';
    String formattedHeureFin =
    (heureFin != null) ? heureFin.format(context) : 'Absent';

    String combinedDateTime =
        '$formattedDateDebut - $formattedDateFin, $formattedHeureDebut - $formattedHeureFin';

    return Container(
      width: 218,
      height: 193,
      margin: const EdgeInsets.only(right: 10),
      child: Card(
        elevation: 4,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(18),
        ),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                event['label'] ?? 'Absent',
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              const SizedBox(height: 8),
              Text(
                combinedDateTime,
              ),
              const SizedBox(height: 8),
              Text(
                'Lieu : ${event['lieu']['localisation'] ?? 'Absent'}',
              ),
            ],
          ),
        ),
      ),
    );
  }
}

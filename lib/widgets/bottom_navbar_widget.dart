import 'package:flutter/material.dart';

class BottomNavBar extends StatefulWidget {
  final bool hasFloatingButton;
  final double iconSpacing;
  final int selectedIndex;
  final Function(int) onItemTapped;
  final bool unselected;

  const BottomNavBar({
    Key? key,
    this.hasFloatingButton = false,
    this.iconSpacing = 0.0,
    required this.selectedIndex,
    required this.onItemTapped,
    this.unselected = false,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _BottomNavBarState();
}

class _BottomNavBarState extends State<BottomNavBar> {
  int get _selectedIndex => widget.selectedIndex;
  bool get _unselected => widget.unselected;


  void _onItemTapped(int index) {
    widget.onItemTapped(index);
  }

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      child: SizedBox(
        height: 60,
        child: widget.hasFloatingButton
            ? Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            IconButton(
              icon: Icon(
                Icons.home,
                color: _selectedIndex == -1 ? Colors.grey : (_selectedIndex == 0 ? const Color(0xFF6AA517) : Colors.grey),
              ),
              onPressed: _unselected ? null : () => _onItemTapped(0),
            ),
            IconButton(
              icon: Icon(
                Icons.favorite,
                color: _selectedIndex == -1 ? Colors.grey : (_selectedIndex == 1 ? const Color(0xFF6AA517) : Colors.grey),
              ),
              onPressed: _unselected ? null : () => _onItemTapped(1),
            ),
            SizedBox(width: 48 + widget.iconSpacing), // Ajouter l'espacement supplémentaire
            IconButton(
              icon: Icon(
                Icons.check_box,
                color: _selectedIndex == -1 ? Colors.grey : (_selectedIndex == 2 ? const Color(0xFF6AA517) : Colors.grey),
              ),
              onPressed: _unselected ? null : () => _onItemTapped(2),
            ),
            IconButton(
              icon: Icon(
                Icons.person,
                color: _selectedIndex == -1 ? Colors.grey : (_selectedIndex == 3 ? const Color(0xFF6AA517) : Colors.grey),
              ),
              onPressed: _unselected ? null : () => _onItemTapped(3),
            ),
          ],
        )
            : BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Accueil',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.favorite),
              label: 'Favoris',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.check_box),
              label: 'Contributions',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profil',
            ),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: _unselected ? Colors.grey : const Color(0xFF6AA517), // Si _unselected est vrai, selectedItemColor est défini sur la même couleur que unselectedItemColor
          unselectedItemColor: Colors.grey, // Couleur des icônes non sélectionnées
          showUnselectedLabels: true, // Affiche les labels des icônes non sélectionnées
          type: BottomNavigationBarType.fixed, // Rend les icônes fixes
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}

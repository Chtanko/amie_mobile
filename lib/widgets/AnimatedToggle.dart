import 'package:flutter/material.dart';

class AnimatedToggle extends StatefulWidget {

  final ValueChanged<bool> onToggle;

  AnimatedToggle({Key? key, required this.onToggle}) : super(key: key);

  @override
  _AnimatedToggleState createState() => _AnimatedToggleState();
}

class _AnimatedToggleState extends State<AnimatedToggle> {
  bool isUpcomingSelected = true;

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        double toggleButtonWidth = constraints.maxWidth * 0.7; // 70% de la largeur totale

        return GestureDetector(
          onTap: () {
            setState(() {
              isUpcomingSelected = !isUpcomingSelected;
            });
            widget.onToggle(isUpcomingSelected);
          },
          child: Container(
            width: toggleButtonWidth,
            height: 35,
            decoration: BoxDecoration(
              color: const Color(0xF4ECEC), // Couleur de fond de la capsule
              borderRadius: BorderRadius.circular(30),
            ),
            child: Stack(
              children: [
                AnimatedPositioned(
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeInOut,
                  left: isUpcomingSelected ? 0 : toggleButtonWidth / 2,
                  child: Container(
                    width: toggleButtonWidth / 2,
                    height: 35,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(30),
                    ),
                    alignment: Alignment.center,
                    child: Text(
                      isUpcomingSelected ? 'À VENIR' : 'TERMINÉ',
                      style: TextStyle(
                        fontSize: 14,
                        color: isUpcomingSelected ? const Color(0xFF4C780E) : Colors.white, // Couleur du texte lorsqu'il est glissé par le bouton
                      ),
                    ),
                  ),
                ),
                Positioned(
                  left: 0,
                  child: Container(
                    width: toggleButtonWidth / 2,
                    height: 35,
                    alignment: Alignment.center,
                    child: const Text(
                      'À VENIR',
                      style: TextStyle(fontSize: 14, color: Color(0xFF4C780E)
                      ), // Couleur du texte initial
                    ),
                  ),
                ),
                Positioned(
                  right: 0,
                  child: Container(
                    width: toggleButtonWidth / 2,
                    height: 35,
                    alignment: Alignment.center,
                    child: const Text(
                      'TERMINÉ',
                      style: TextStyle(fontSize: 14, color: Color(0xFF4C780E)
                      ), // Couleur du texte initial
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import '../views/event/create_event.dart';

class FloatingActionButtonWidget extends StatelessWidget {
  const FloatingActionButtonWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () {
        Get.to(() => const CreateEvent());
      },
      backgroundColor: const Color(0xFF4C780E),
      foregroundColor: Colors.white,
      child: const Icon(Icons.add),
    );
  }
}
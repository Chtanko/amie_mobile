import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class EventWidget {
  static String getFormattedDateTimeString(Map<String, dynamic> event, BuildContext context) {
    DateTime? dateDebut;
    DateTime? dateFin;
    try {
      dateDebut = DateFormat('dd/MM/yyyy').parse(event['date_debut']);
      dateFin = DateFormat('dd/MM/yyyy').parse(event['date_fin']);
    } catch (e) {
      print("Erreur lors de la conversion de la date: $e");
    }

    TimeOfDay? heureDebut;
    if (event['heure_debut'] != null &&
        event['heure_debut']
            .contains(RegExp(r'^([01]?[0-9]|2[0-3]):[0-5][0-9]$'))) {
      heureDebut = TimeOfDay.fromDateTime(
          DateTime.parse("1970-01-01 " + event['heure_debut']));
    } else {
      print('Erreur de format pour l\'heure de début');
    }

    TimeOfDay? heureFin;
    if (event['heure_fin'] != null && event['heure_fin'].contains(RegExp(r'^([01]?[0-9]|2[0-3]):[0-5][0-9]$'))) {
      heureFin = TimeOfDay.fromDateTime(
          DateTime.parse("1970-01-01 " + event['heure_fin']));
    } else {
      print('Erreur de format pour l\'heure de fin');
    }

    String formattedHeureDebut = (heureDebut != null) ? heureDebut.format(context) : 'A venir';
    String formattedHeureFin = (heureFin != null) ? heureFin.format(context) : 'A venir';

    String formattedDateDebut = (dateDebut != null)
        ? DateFormat('d MMM yyyy', 'fr_FR').format(dateDebut) : 'A venir';
    String formattedDateFin = (dateFin != null) ? DateFormat('d MMM yyyy', 'fr_FR').format(dateFin) : 'A venir';

    if (dateDebut != null && dateFin != null) {
      if (dateDebut.day == dateFin.day && dateDebut.month == dateFin.month && dateDebut.year == dateFin.year) {
        // Si la date de début et de fin est la même
        formattedDateDebut = DateFormat('d MMM yyyy', 'fr_FR').format(dateDebut);
        formattedDateFin = "";
      } else if (dateDebut.month == dateFin.month && dateDebut.year == dateFin.year) {
        // Si c'est le même mois
        formattedDateDebut = DateFormat('d', 'fr_FR').format(dateDebut);
        formattedDateFin = DateFormat('-d MMM yyyy', 'fr_FR').format(dateFin);
      } else {
        // Si c'est un événement qui dure plusieurs mois
        formattedDateDebut = DateFormat('d MMM yyyy', 'fr_FR').format(dateDebut);
        formattedDateFin = DateFormat('-d MMM yyyy', 'fr_FR').format(dateFin);
      }
    }

    // Gestion spéciale si dateDebut et dateFin sont null
    if (dateDebut == null && dateFin == null) {
      formattedDateDebut = 'A venir';
      formattedDateFin = '';
    }

    String combinedDateTime =
        '$formattedDateDebut$formattedDateFin, $formattedHeureDebut - $formattedHeureFin';

    return combinedDateTime;
  }
}

